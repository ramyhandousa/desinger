<?php

use App\Models\Profile;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UserSeeder extends Seeder
{

    public function run(Faker $faker)
    {
        //php artisan db:seed --class=UserSeeder
        factory(App\User::class, 10)->create()->each(function ( $user)  {

            $user->profile()->save(factory( Profile::class)->make());

        });
    }

}
