<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->enum('defined_order',['designer','building'])->default('designer');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('provider_id')->nullable();
            $table->text('description');
            $table->string('images');
            $table->string('app_percentage')->nullable();
            $table->tinyInteger('is_review')->default('0');
            $table->tinyInteger('is_pay')->default('0');
            $table->tinyInteger('time_out')->default('0');
            $table->enum('payment',['cash','online'])->default('cash');
            $table->string('paymentId');
            $table->enum('status', ['pending','empty', 'accepted','finish','refuse'])->default('pending');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('provider_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
