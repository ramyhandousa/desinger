<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->enum('defined_user',['admin','user','designer'])->default('user');
            $table->string('name');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->tinyInteger('is_active')->default(0);
            $table->tinyInteger('is_accepted')->default(0);
            $table->boolean('is_payed')->default(0);
            $table->tinyInteger('is_suspend')->default(0);
            $table->string('message')->nullable();
            $table->string('lang')->default('ar');
            $table->string('api_token');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
