<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'desc' => $faker->text,
        'experience_year' => rand(1,2),
        'cv' => rand(1,2),
        'images_work' => ["1","2"],
    ];
});
