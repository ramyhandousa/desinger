<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AcceptAdminPaymentNotify
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $user;
    public $order;
    public $request;


    public function __construct( $user, $order, $request)
    {
        $this->user = $user;
        $this->order = $order;
        $this->request = $request;

    }
}
