<?php

namespace App\Events;

use App\Models\Order;
use App\Models\OrderProject;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewProjectNotify
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $provider;
    public $order;
    public $request;


    public function __construct(User $user,User $provider,  Order $order, $request)
    {
        $this->user = $user;
        $this->provider = $provider;
        $this->order = $order;
        $this->request = $request;

    }
}
