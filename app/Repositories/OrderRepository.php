<?php


namespace App\Repositories;


use App\Events\NewOrderOffer;
use App\Events\OrderStatus;
use App\Http\Resources\Order\OrderIndex;
use App\Models\Order;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class OrderRepository implements OrderRepositoryInterface
{

    public function list($request)
    {
        $order =  $request->user()->orders()->select('id','defined_order','description','status','created_at');

        if ($request->pending){
            $data = $order->whereStatus('pending');

        }elseif ($request->empty){
            $data = $order->whereStatus('empty');

        }elseif ($request->accepted){

            $data = $order->whereStatus('accepted');

        }elseif ($request->finish){
            $data = $order->whereStatus('finish');

        }else{
            $data = $order;
        }

        $this->pagination_query($request, $data);

        return OrderIndex::collection($data->get());
    }

    public function show($request)
    {
        // TODO: Implement show() method.
    }

    public function makeOrder($request)
    {
      $order =   $request->user()->orders()->create($request->validated());

      event(new NewOrderOffer($request->user(),$order, $request));
    }

    public function updateOrder($order , $request)
    {
      event(new NewOrderOffer($request->user(),$order, $request));

      $order->update(['status' => 'pending']);
    }

    public function rateOrder($request , $order){
        $user = Auth::user();

        $order = $order->load('provider','user');

        $provider = $order['provider'];

        $rate =  $provider->rating->where('id', $user->id)->where('pivot.order_id',$order->id)->first();

        $message = $request->message ? : null;
        if ($rate){
            $rate->pivot->update(['rate' => $request->rate, 'message' => $message]);

        }else{
            $provider->rating()->attach( $user->id ,['provider_id' => $provider->id,'order_id' => $order->id , 'rate' => $request->rate, 'message' => $message]);
        }

        event(new OrderStatus($provider,$order['user'],$request,$order,11));
    }


    public function pagination_query($request ,$query ){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1
        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
        return $query->latest();
    }

}
