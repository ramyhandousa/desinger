<?php


namespace App\Repositories\Interfaces;

interface  AuthRepositoryInterface
{

    public function register($request);

    public function login($request);

    public function forgetPassword($request);

    public function resetPassword($request);

    public function checkCode($request);

    public function resendCode($request);

    public function changPassword($request);

    public function editProfile($request);

    public function logOut($request);

}
