<?php


namespace App\Repositories;

use App\Events\PhoneOrEmailChange;
use App\Events\UserLogOut;
use App\Http\Helpers\Images;
use App\Http\Resources\User\UserCodeActivation;
use App\Http\Resources\User\UserResource;
use App\Models\Device;
use App\Models\Profile;
use App\Models\UploadImage;
use App\Models\VerifyUser;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthRepository implements AuthRepositoryInterface
{

    public  $path;
    public function __construct( )
    {
        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');

        $this->path = 'files/';
    }

    public function register($request)
    {
        $action_code = substr(rand(), 0, 4);

        $user = User::create($request->only(['name', 'phone','code_country', 'email', 'password']));

        $this->createVerfiy($request, $user, $action_code);

        $this->manageDevices($request, $user);

        return ['code' => $action_code, 'api_token' => $user->api_token];
    }

    public function registerEngineer($request){

        $user = Auth::user();

        Profile::updateOrCreate(['user_id' => $user->id ], $request->validated());
    }

    public function login($request)
    {
        $user =  Auth::user();

        $this->manageDevices($request, $user);

        return new UserResource($user);
    }

    public function forgetPassword($request)
    {
        $user = User::whereEmail($request->provider)->orWhere('phone', $request->provider)->first();

        $columnChange = filter_var($request->provider, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $action_code = substr(rand(), 0, 4);

        event(new PhoneOrEmailChange($user,$action_code,$columnChange,$request->provider));

        $message = $columnChange == 'email' ? trans('global.activation_code_sent_email')  :  trans('global.activation_code_sent');

        return ['code' => $action_code ,'message' => $message];
    }

    public function resetPassword($request)
    {
        $user = User::whereEmail($request->provider)->orWhere('phone', $request->provider)->first();

        if ($request->password){

            $user->update(['password' => $request->password]);

            $data = new UserResource($user);

            return  [ 'data' => $data , 'message' => trans('global.password_was_edited_successfully') ];

        }else{
            $data = new UserResource($user);

            return  [ 'data' => $data , 'message' => trans('global.password_not_edited') ];
        }
    }

    public function checkCode($request)
    {
        $verifyUser = VerifyUser::whereEmail($request->provider)->orWhere('phone', $request->provider)->with('user')->first();

        $user = $verifyUser['user'];

        $phone = $verifyUser->phone ? : $user->phone;
        $email = $verifyUser->email ? : $user->email ;
        $code  = $verifyUser->code_country ?: $user->code_country;

        $verifyUser['user']->update(['phone' => $phone , 'email' => $email,'code_country' => $code,  'is_active' => 1]);

        $verifyUser->delete();

        $this->manageDevices($request, $verifyUser['user']);

        return new UserResource($verifyUser['user']);
    }

    public function resendCode($request)
    {
        $user = VerifyUser::whereEmail($request->provider)->orWhere('phone', $request->provider)->with('user')->first();

        $columnChange = filter_var($request->provider, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $action_code = substr(rand(), 0, 4);

        event(new PhoneOrEmailChange($user['user'],$action_code,$columnChange,$request->provider));

        $message = $columnChange == 'email' ? trans('global.activation_code_sent_email')  :  trans('global.activation_code_sent');

        return ['code' => $action_code ,'message' => $message];
    }

    public function changPassword($request)
    {
        $user = Auth::user();

        if ($request->newPassword){

            $user->update( [ 'password' => $request->newPassword,'password_change_at' => Carbon::now() ] );

            $message =  trans('global.password_was_edited_successfully') ;

        }else{

            $message = trans('global.password_not_edited');
        }

        return  ['message' => $message] ;
    }

    public function editProfile($request)
    {
        $user = Auth::user();
        $user->name = $request->name;
        if ($request->image){
            $user->image = $this->uploadProfileImage($request);
        }
        $user->save();

        return new UserResource($user);
    }
    public function editEmail($request)
    {
        $user = Auth::user();

        $action_code = substr(rand(), 0, 4);

        $columnChange = 'email';
        $requestChange = $request->email;

        $columnExists = $user->where($columnChange, $requestChange)->exists();

        if (!$columnExists && $requestChange){

            event(new PhoneOrEmailChange($user,$action_code,$columnChange,$requestChange));

            $message = trans('global.activation_code_sent_email');
        }else{
            $message = trans('global.profile_not_edit_success');
        }
        $user->load('code');

        return  ['data' => new UserCodeActivation($user) , 'message' => $message ];
    }
    public function editPhone($request)
    {
        $user = Auth::user();

        $action_code = substr(rand(), 0, 4);

        $columnChange = 'phone';
        $requestChange = $request->phone;

        $columnExists = $user->where($columnChange, $requestChange)->exists();

        if (!$columnExists && $requestChange){

            event(new PhoneOrEmailChange($user,$action_code,$columnChange,$requestChange));

            $message = trans('global.activation_code_sent');
        }else{
            $message = trans('global.profile_not_edit_success');
        }
        $user->load('code');

        return  ['data' => new UserCodeActivation($user) , 'message' => $message ];
    }

    public function logOut($request)
    {
        $user = Auth::user();

        event(new UserLogOut($user,$request));
    }

    public function manageDevices($request, $user)
    {
        if ($request->deviceToken) {

            $device = Device::where('device', $request->deviceToken)->first();

            if (!$device) {
                $data = new Device();
                $data->device = $request->deviceToken;
                $data->user_id = $user->id;
                $data->device_type = $request->deviceType;
                $data->save();
            }else{
                $device->update(['user_id' => $user->id , 'device' => $request->deviceToken ]);
            }
        }
    }

    public function createVerfiy($request , $user , $action_code){
        $verifyPhone = new VerifyUser();
        $verifyPhone->user_id       =    $user->id;
        $verifyPhone->code_country  =    $request->code_country ?: $user->code_country;
        $verifyPhone->phone         =    $request->phone;
        $verifyPhone->email         =    $request->email;
        $verifyPhone->action_code   =    $action_code;
        $verifyPhone->save();
    }


    public function uploadProfileImage($request ){

        $image = UploadImage::where('id',$request->image)->first();

        return $image->image;
    }

    public function uploadImage($request){

        $this->deleteOldImage($request);

        $image = new UploadImage();

        $imageName = $filename = time() . '.' . Str::random(20) . $request->image->getClientOriginalName();
        $request->image->move(public_path('files'), $imageName);

        $image->image = $this->path.$imageName;
        $image->save();

        return [ 'id' => $image->id];
    }

    public function removeImage($request){

        $this->deleteOldImage($request);
    }

    function deleteOldImage($request){

        if ($request->image == 1){
            $this->deleteUserImage();
        }

        if ($request->oldId){
            $last = UploadImage::whereId($request->oldId)->first();

            if ($last){
                if ($last->image){
                    File::delete(public_path().'/'.$last->image);
                }

                $last->delete();
            }

        }
    }


    function deleteUserImage(){
        $user = Auth::user();

        if ($user->image){
            $user->update(['image' => null ]);
        }
    }
}
