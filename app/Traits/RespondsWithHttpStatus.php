<?php


namespace App\Traits;


trait  RespondsWithHttpStatus
{
    protected function success($message, $data = [], $status = 200)
    {
        return response([
            'status' => 200,
            'message' => $message,
            'data' => $data,
        ], $status);
    }

    protected function failure($error = [], $status = 400)
    {
        return response([
            'status' => $status,
            'error' => $error,
        ], 200);
    }

}
