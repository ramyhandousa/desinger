<?php

namespace App\Policies;

use App\Models\Order;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Order  $order
     * @return mixed
     */
    public function view(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Order  $order
     * @return mixed
     */
    public function update(User $user, Order $order)
    {

        return ( $user->id === $order->user_id )
            ? Response::allow()
            : Response::deny('تأكد من انك الشخص المستحق لإعادة إرسال الطلب  ');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Order  $order
     * @return mixed
     */
    public function delete(User $user, Order $order)
    {
        return ( $user->id === $order->user_id )
            ? Response::allow()
            : Response::deny('تأكد من انك الشخص المستحق لمسح الطلب  ');
    }



    public function rate(User $user, Order $order)
    {

        return ( $user->id === $order->user_id )
            ? Response::allow()
            : Response::deny('تأكد من انك الشخص المستحق لتقيم الطلب  ');
    }

    public function complete(User $user, Order $order)
    {

        return ( $user->id === $order->user_id )
            ? Response::allow()
            : Response::deny('تأكد من انك الشخص المستحق لأعتماد الطلب  ');
    }

    public function showOffer(User $user, Order $order)
    {

        return ( $user->id === $order->user_id )
            ? Response::allow()
            : Response::deny('تأكد من انك الشخص المستحق لمشاهدة  عروض الطلب  ');
    }
}
