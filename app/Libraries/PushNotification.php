<?php

namespace App\Libraries;

use App\Libraries\FirebasePushNotifications\Firebase;
use App\Libraries\FirebasePushNotifications\Push;

class PushNotification
{
    
    public function sendPushNotification($regIdsAndroid = null, $regIdsIos = null,  $title = null, $body = null, $data = [])
    {
        $push = new Push();
        $firebase = new Firebase();

        $dataLoad = array();
        $dataLoad['href'] = isset($data['href']) ? $data['href'] : "";

        isset($data['id'])              ?  $push->setId($data['id'])                    : 0;
        isset($data['user_id'])         ?  $push->setUserId($data['user_id'])               : 0;
        isset($data['user_image'])      ?  $push->setUserImage($data['user_image'])               : " ";
        isset($data['conversation_id']) ?  $push->setConversationId($data['conversation_id'])       : 0;
        isset($data['message'])         ?  $push->setMessage($data['message'])               :" ";
        isset($data['fileName'])        ?  $push->setFileName($data['fileName'])              : " ";
        isset($data['url'])             ?  $push->setUrl($data['url'])                   : " ";
        isset($data['is_read'])         ?  $push->setIsRead($data['is_read'] )          : 0;
        isset($data['type'])            ?  $push->setType($data['type'] ?: 0)           : 0;
        isset($data['orderId'])         ?  $push->setOrderId($data['orderId'])          : 0;
        isset($data['order_type'])      ?  $push->setOrderType($data['order_type'])     : 0;
        isset($data['product_id'])      ?  $push->setProductId($data['product_id'])     : 0;
        isset($data['created_at'])      ?  $push->setCreatedAt($data['created_at'])     :  date('Y-m-d G:i:s');
        isset($data['image'])           ?  $push->setImage($data['image'])              :  "https://ashel-app.com/public/Site/images/logo.png";

        // notification title
        $push->setTitle($title);

        $push->setMessage($body);

        $push->setIsBackground(TRUE);

        $push->setData($dataLoad);

        $responseIos = '';
        $responseAndroid = '';

        if (collect($regIdsAndroid)->count() >  0) {
            $json = $push->getPushData();
            $push = $push->getPushNotification();
            $responseAndroid = $firebase->sendMultipleNotificationAndData($regIdsAndroid, $push , $json);
//            $responseAndroid = $firebase->sendMultipleAndroid($regIdsAndroid , $json);
        }

        if( collect($regIdsIos)->count() >  0 ){
            $push = $push->getPushNotification();
            $responseIos = $firebase->sendMultipleIos($regIdsIos, $push);
        }

        return [$responseAndroid, $responseIos];
    }

}