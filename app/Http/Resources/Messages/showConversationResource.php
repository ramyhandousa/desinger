<?php

namespace App\Http\Resources\Messages;

use App\Http\Resources\User\UserImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class showConversationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $sender = $this->users()->where('id','!=',auth()->id())->first();
        return [
            'id'                 => $this->id,
            'open'               =>  $this->status == 'open' ,
            'batches_remaining'  => $this->order->batches_remaining,
            'user'               =>   new UserImageResource($sender),
            'messages'           =>  $this->paginationLastMessages($request,$sender)
        ];
    }



    function paginationLastMessages($request,$sender){

        $last_messages = $this->last_messages->where('user_id',$sender->id);

        $pageSize = $request->pageSize;
        $skipCount = $request->skipCount;

        if ($pageSize || $skipCount){

            $currentPage = $request->get('page', 1);

            $data = $last_messages->slice($skipCount + (($currentPage - 1) * $pageSize))->take($pageSize)->values();

        }else{
            $data = $last_messages->take(10);
        }

        return showMessagesResource::collection( $data);
    }
}
