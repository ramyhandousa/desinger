<?php

namespace App\Http\Resources\Order;

use App\Models\UploadImage;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderShow extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'defined_order' => $this->defined_order,
            'description'   => $this->description,
            'images'        => UploadImage::whereIn('id',$this->images)->get(['id','image']),
            'time_out'      => $this->time_out,
            'offer_count'      => $this->offers->whereNotNull('price')->count(),
        ];
    }
}
