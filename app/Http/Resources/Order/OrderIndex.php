<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderIndex extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'defined_order' => $this->defined_order,
            'description'   => $this->description,
            'status'        => $this->status,
            'status_translation'        => $this->status_translation,
            'time'          => $this->when($this->created_at  ,  $this->created_at? $this->created_at->diffForHumans(): null),
        ];
    }
}
