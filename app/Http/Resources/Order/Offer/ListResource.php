<?php

namespace App\Http\Resources\Order\Offer;

use Illuminate\Http\Resources\Json\JsonResource;

class ListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $percentage = $this->batches !== 0 ? ( ($this->price / $this->batches) / $this->price ) * 100 : 0;

        return [
            'id'                => $this->id,
            'provider_name'     => $this->provider->name,
            'provider_image'    => $this->provider->imageProfile(),
            'price'             => $this->price,
            'total_time'        => $this->total_time,
            'batches'           => $this->batches,
            'duration'          => $this->duration,
            'the_value_of_each_batch' => $percentage  == 0 ? 0 . '%' : number_format( $percentage,0) . '%' ,
        ];
    }
}
