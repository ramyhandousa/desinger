<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserCodeActivation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            $this->mergeWhen( $this->code && $this->code->email !== null && $request->email && $request->route()->uri ==  'api/v1/Auth/editEmail',[
                'code'          => $this->when($this->code, optional($this->code)->action_code),
            ]),
            $this->mergeWhen( $this->code && $this->code->phone !== null&& $request->phone && $request->route()->uri ==  'api/v1/Auth/editPhone',[
                'code'          => $this->when($this->code, optional($this->code)->action_code),
            ])
        ];
    }
}
