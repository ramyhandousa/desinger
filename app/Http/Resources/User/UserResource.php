<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'            => $this->id,
            'user'          => $this->defined_user,
            'name'          => $this->name,
            'image_profile' => $this->when($this->image, URL('/') .'/'. $this->image)  ,
            'phone'         => $this->phone,
            'code_country'  => $this->code_country,
            'email'         => $this->email,
            'api_token'     => $this->when($this->api_token , $this->api_token) ,
            'message'       => $this->when($this->message , $this->message) ,
            'is_active'     => $this->is_active ? 1  == true : false,
            'is_suspend'    => $this->is_suspend ? 1  == true : false,
            'is_accepted'    => $this->is_accepted ? 1  == true : false,
        ];


    }
}
