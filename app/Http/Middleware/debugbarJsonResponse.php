<?php

namespace App\Http\Middleware;

use Closure;
use DebugBar\DebugBar;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;

class debugbarJsonResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response =  $next($request);


        if ($response instanceof  JsonResponse && Config::get('app.debug') && $request->has('_bola')  ){

          $response->setData($response->getData(true) + [
                '_bola' => Arr::only( DebugBar()->getData() ,  'queries')
              ]);
        }

        return $response;
    }
}
