<?php

namespace App\Http\Requests\api\Order;

use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class storeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'defined_order'       => 'required|in:designer,building',
            'description'   => 'required|string|min:10|max:1000',
            'images'        => 'required|array',
            'images.*'      => 'exists:upload_images,id',
        ];
    }

    public function messages()
    {
        return [
            'images.required' => 'مطلوب الصور المرفقة ',
            'images.array' => 'array   تأكد من ان الصور المرفقة  في شكل  ',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $designer = User::whereDefinedUser('designer')->whereIsAccepted(1)->whereIsActive(1)
                    ->whereIsSuspend(0)->whereHas('profile')->exists();

            if (! $designer ) {
                $validator->errors()->add('unavailable', 'للأسف لا يوجد مهندسين حاليا لإستقبال طلبك');
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }

}
