<?php

namespace App\Http\Requests\Admin;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class updateWork extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'master_image' => 'image|mimes:jpeg,png,jpg|max:10240',
            'image.*' => 'image|mimes:jpeg,png,jpg|max:10240',
        ];
    }

    public function messages()
    {
        return [
            'master_image.image' => 'تاكد من صورة العمل الرئيسية صورة فقط ',
            'master_image.mimes' => '    تأكد من صورة العمل الرئيسية ان لا تكون سوي  jpeg,png,jpg ',
            'images.*.image' => 'تاكد من انك تضيف صور فقط ',
            'images.*.mimes' => '    تأكد من ان الصور المرفقة لا تكون سوي  jpeg,png,jpg ',
        ];
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 422));
    }
}
