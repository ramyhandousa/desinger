<?php

namespace App\Http\Controllers\Admin;

use App\Events\AcceptAdminPaymentNotify;
use App\Events\RefuseAdminPaymentNotify;
use App\Http\Controllers\Controller;
use App\Models\BankTransfer;
use App\Models\Conversation;
use App\Models\UploadImage;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BankTransferController extends Controller
{
   public function index(){
       $banks = BankTransfer::latest()->get();
       $banks->map(function ($q){
           $image = UploadImage::whereId($q->image)->first();
           $q->image =  $q->image ?  $image->image : null;
       });


//       $banks = BankTransfer::distinct('order_id')->get(['order_id']);
//       $banks->load('order');

       $pageName = 'التحويلات البنكية' ;

       return view('admin.bankTransfer.index',compact('banks','pageName'));
   }

    public function accepted(Request $request){

        $bank = BankTransfer::with('user','order','payment')->whereId($request->id)->first();

        $order = $bank['order'];

       if ($order ){
          if ($order->is_pay == 0){
              $this->updateOrder($order,$bank);
          }
          $batches_remaining = $order->batches_remaining  !== 0 ? $order->batches_remaining - 1 : 0;
          $order->update(['batches_remaining' =>  $batches_remaining ]);
          $this->updateNextDateTransfer($order,null);
       }

        $payment = $bank['payment'];

        if ($payment){
            $payment->update(['is_review' => 1]);
        }

        $bank->update(['is_accepted'=> 1]);

        event(new AcceptAdminPaymentNotify($bank['user'],$bank['order'],$request));
        return response()->json([
            'status'=>true,
            'title'=>"نجاح",
            'message'=>"تم قبول الطلب بنجاح",
        ]);
    }

    function updateOrder($order , $bank){

        $order->update(['is_pay' => 1 ]);

        $conversation = new Conversation();
        $conversation->order_id = $order->id;
        $conversation->save();
        $conversation->users()->attach([$bank['user']->id, $order->provider_id]);
    }

    public function refuse(Request $request){

        $bank = BankTransfer::with('user','order','payment')->whereId($request->id)->first();

        event(new RefuseAdminPaymentNotify($bank['user'],$bank['order'],$request));

        $bank->update(['is_accepted' =>  -1 ,'message' => $request->refuse_reason]);

        $payment = $bank['payment'];

        if ($payment){
            $payment->delete();
        }
        session()->flash('success','تم رفض الطلب بنجاح');
        return redirect()->back();
    }


    function updateNextDateTransfer($order , $offer){

        if ($offer){

            $masterOffer = $offer;
        }else{

            $masterOffer =  $order->offers->where('order_id',$order->id)->first();
        }
        $duration  = $masterOffer->duration  ;

        if ($duration !== 0){

            $next_transfer =  $order->next_transfer !== null ? Carbon::parse($order->next_transfer)->addDay($duration) : Carbon::today()->addDay($duration);

            $order->update(['next_transfer' => $next_transfer]);
        }
    }


}
