<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\order\OrderRateRequest;
use App\Http\Requests\api\Order\storeRequest;
use App\Http\Requests\api\Order\updateOrderVaild;
use App\Http\Resources\Order\OrderShow;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use RespondsWithHttpStatus;
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;

        $this->middleware('auth:api');
    }

    public function index(Request $request){
        $data = $this->orderRepository->list($request);

        return $this->success('طلباتك', $data);
    }

    public function show(Order $order){

        $data =  new OrderShow($order);
        return $this->success('تفاصيل الطلب', $data);
    }
    public function store(storeRequest $request){

        $this->orderRepository->makeOrder($request);

        return $this->success('تم إرسال طلبك إلي المهندسين بنجاح');
    }

    public function update(  updateOrderVaild $updateOrderVaild, Order $order){

        $this->authorize('update', $order);

        $this->orderRepository->updateOrder($order, $updateOrderVaild);

        return $this->success('تم إعادة إرسال طلبك إلي المهندسين بنجاح');
    }

    public function destroy(Order $order){

        $this->authorize('delete', $order);

        $order->delete();

        return $this->success('تم مسح طلبك بنجاح');
    }

    public function rateOrder(OrderRateRequest $request, Order $order){

        $this->authorize('rate', $order);

        $this->orderRepository->rateOrder($request, $order);

        return $this->success( trans('global.rate_success'));
    }
}
