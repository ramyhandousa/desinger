<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\HomeScreen;
use App\Models\PreviousWork;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use RespondsWithHttpStatus;

    public function index(Request $request){
        $query = PreviousWork::select('id','image');

        $this->pagination_query($request, $query);

        $works = $query->get();

        return $this->success('بعض من أعمالنا', $works);
    }

    public function show(PreviousWork $previousWork){
       $model = $previousWork->load('images');

       return $this->success('بعض من أعمالنا', new HomeScreen($model));
    }


    public function pagination_query($request ,$query ){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1
        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
        return $query->latest();
    }
}
