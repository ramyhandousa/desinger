<?php

namespace App\Http\Controllers\Api;

use App\Events\ContactUs;
use App\Events\NewBankTransfer;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\setting\bankTransferRequest;
use App\Http\Requests\api\setting\contactVaild;
use App\Http\Resources\setting\BankResource;
use App\Http\Resources\Setting\typeSupportResource;
use App\Models\Bank;
use App\Models\BankTransfer;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypeSupport;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{

    use RespondsWithHttpStatus;
    public function __construct( )
    {
        $this->middleware('auth:api')->only(['bank_transfer','contact_us']);
    }

    public function banks(){

        $data = BankResource::collection(Bank::all());

        return $this->success('الحسابات البنكية',$data);
    }


    public function bank_transfer(bankTransferRequest $request){

        $user = Auth::user();
        $bankTransfer = new BankTransfer();
        $bankTransfer->user_id = $user->id;
        $bankTransfer->bank_id  = $request->bankId;
        $bankTransfer->save();

//        event(new NewBankTransfer($user));

        return $this->success( trans('global.success_progress'));
    }

    public function aboutUs(){
        $about_us =  Setting::where('key','about_us_ar')->first();

        $data =  $about_us ? $about_us->body : '' ;

        return $this->success('عن التطبيق ', $data);
    }

    public function terms_user(){
        $setting = Setting::where('key','terms_user_ar')->first();

         $data =  $setting ? $setting->body : '' ;

        return $this->success( 'الشروط والأحكام', $data);
    }


    public function getTypesSupport(){

        $types = TypeSupport::all();

        $data= typeSupportResource::collection($types);

        return $this->success( 'أنواع التواصل', $data);
    }

    public function contact_us(contactVaild $request ){
        $user = Auth::user();

        $support = new Support();
        $support->user_id = 1;
        $support->sender_id = $user->id;
        $support->type_id = (int) $request->type;
        $support->message = $request->message;
        $support->save();

        event(new ContactUs(1,$user->id,$request));

        return $this->success( trans('global.message_was_sent_successfully'));
    }

}
