<?php

namespace App\Http\Controllers\Api;

use App\Events\ChattingUserNotify;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\message\storeVaild;
use App\Http\Resources\Messages\showConversationResource;
use App\Models\Conversation;
use App\Models\Message;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    use RespondsWithHttpStatus;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $user = Auth::user()->load(['conversations.users','conversations.last_messages']);

        $conversations = $user->conversations->indexData();

        $pageSize = $request->pageSize;

        $currentPage = $request->get('page', 1);

        $data = $conversations->slice($request->skipCount + (($currentPage - 1) * $pageSize))->take($pageSize)->values();

        return $this->success('المحادثات', $data);
    }

    public function store(storeVaild $request)
    {
        $user = Auth::user()->load('conversations.users');

        $data =  $request->validated();

        $data['user_id']  = $user->id;

        $message = Message::create($data);

        event(New ChattingUserNotify($user,$message,$request));

        return $this->success('تم الإرسال بنجاح', $message);
    }


    public function show(Conversation $conversation)
    {
        $showConversation =  $conversation->load('order','last_messages','users');

        $data = new showConversationResource($showConversation);

        return $this->success('الشات', $data);
    }


    public function complete(Conversation $conversation)
    {
        $conversation->load('order');

        $order = $conversation['order'];

        $this->authorize('complete', $order);

        $conversation->update(['status' => 'close']);

        $order->update(['status' => 'finish']);

        return $this->success('تم إعتماد طلبك بنجاح');
    }
}
