<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\offer\vaildListOffer;
use App\Http\Resources\Order\Offer\ListResource;
use App\Models\Order;
use App\Models\OrderOffer;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OfferController extends Controller
{
    use RespondsWithHttpStatus;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(vaildListOffer $vaildListOffer,Order $order){

        $this->authorize('showOffer', $order);

        $my_order = Auth::user()->orders()->whereId($order->id)->first();

        $offers = $my_order ? $my_order->offers->whereNotNull('price') : [];

        $data = ListResource::collection($offers);

        return  $this->success('الإطلاع علي العروض',$data );
    }

    public function acceptedOffer(Request $request, OrderOffer $orderOffer){

        $user = Auth::user();

        $offer = $orderOffer->load('order');

        $order =  $offer['order']->whereStatus('pending')->first();

        if (!$order){  return $this->OfferAcceptedBefore();   }

        $order->update(['provider_id' =>  $offer->provider_id,'total_price' => $offer->price,'batches_remaining' => $offer->batches ,'status' => 'accepted']);

        $delete = $offer->where('provider_id' ,'!=' ,  $offer->provider_id )->get();

        if (count($delete) > 0){
            $delete->each->delete();
        }
//        $this->notify->NotificationDbType(7,$order->provider_id,$user, $offer,$order);

        return   $this->success(__('global.accepted_order'));
    }

      function OfferAcceptedBefore(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا العرض تم قبوله من قبل '   ],200);
    }

}
