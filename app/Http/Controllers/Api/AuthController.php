<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\Auth\changePassRequest;
use App\Http\Requests\api\Auth\checkActivation;
use App\Http\Requests\api\Auth\checkPhoneOrEmailExist;
use App\Http\Requests\api\Auth\codeExist;
use App\Http\Requests\api\Auth\editPhone;
use App\Http\Requests\api\Auth\editUser;
use App\Http\Requests\api\Auth\login;
use App\Http\Requests\api\Auth\providerVaild;
use App\Http\Requests\api\Auth\resgister;
use App\Http\Requests\api\Auth\validEngineer;
use App\Http\Requests\api\removeImage;
use App\Http\Requests\api\validImage;
use App\Repositories\AuthRepository;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use RespondsWithHttpStatus;
    private $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;

        $this->middleware('auth:api')
            ->only(['registerEngineer' ,'changPassword' ,'editProfile' ,
                'editEmail' ,'editPhone' ,'removeImage','logOut','removeEngineer']);
    }

    public function register(resgister $request){

       $data =  $this->authRepository->register($request);

       return $this->success(trans('global.register_in_successfully'),$data);
    }

    public function registerEngineer(validEngineer $request){

          $this->authRepository->registerEngineer($request);

       return $this->success('تم طلب إنضمامك كمهندس بنجاح');

    }

    public function login(login $request){

        $data =   $this->authRepository->login($request);

        return $this->success(trans('global.logged_in_successfully'),$data);
    }

    public function forgetPassword(checkPhoneOrEmailExist $request){

        $data =    $this->authRepository->forgetPassword($request);


        return $this->success($data['message'],['code' => $data['code'] ] );
    }

    public function resetPassword(checkPhoneOrEmailExist $request){

        $data =    $this->authRepository->resetPassword($request);

        return $this->success($data['message'],$data['data']);
    }

    public function resendCode(codeExist $request){

        $data =    $this->authRepository->resendCode($request);

        return $this->success($data['message'],$data['code']);
    }

    public function checkCodeActivation(checkActivation $request){

        $data =    $this->authRepository->checkCode($request);

        return $this->success(trans('global.your_account_was_activated'),$data);
    }


    public function changPassword ( changePassRequest  $request )
    {
        $data =    $this->authRepository->changPassword($request);

        return $this->success($data['message']);
    }


    public function editProfile (editUser $request )
    {

        $data =  $this->authRepository->editProfile($request);

        return $this->success(trans('global.profile_edit_success') , $data);
    }

    public function editEmail (editUser $request )
    {

        $data =  $this->authRepository->editEmail($request);

        return $this->success($data['message'] , $data['data']);
    }

    public function editPhone (editPhone $request )
    {
        $data =  $this->authRepository->editPhone($request);

        return $this->success($data['message'] , $data['data']);
    }


    public function logOut(Request $request){

         $this->authRepository->logOut($request);

        return $this->success(trans('global.logged_out_successfully'));
    }

    public function upload(validImage $request){

        $data =  $this->authRepository->uploadImage($request);

        return $this->success('تم رفع الصورة بنجاح',$data);
    }

    public function removeImage(removeImage $request){

        $this->authRepository->removeImage($request);

        return $this->success('تم مسح الصورة بنجاح');
    }

    public function removeUser(Request  $request){

        $user = User::whereEmail($request->provider)->orWhere('phone', $request->provider)->first();

        if ($user){
            $user->delete();

            return $this->success('تم مسح المستخدم بنجاح');
        }else{
            return $this->success('لا يابشمهندس البيانات مش صح  😂😂');
        }
    }

    public function removeEngineer(Request  $request){

        $user = Auth::user();

        $profile =  $user->profile;

        if ($profile){
            $profile->delete();

            return $this->success('تم مسح طلب الإنضمام بنجاح');
        }else{
            return $this->success('إتاكد علشان نمسح علي طول تك بوم  😂😂');
        }
    }
}
