<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use RespondsWithHttpStatus;

    public function __construct()
    {
        $this->middleware('auth:api')->only([]);
    }

    public function show_user($id){

        $user = User::findOrfail($id);

        return $this->success('بيانات المستخدم',new  UserResource($user));
    }
}
