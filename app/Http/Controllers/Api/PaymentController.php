<?php

namespace App\Http\Controllers\Api;

use App\Events\NewBankTransfer;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Images;
use App\Http\Requests\api\payment\payOrderVaild;
use App\Models\BankTransfer;
use App\Models\Conversation;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderOffer;
use App\Models\Payment;
use App\Traits\RespondsWithHttpStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    use RespondsWithHttpStatus;
    public  $path;
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->path = 'files/';
    }


    public function payOrder(payOrderVaild $request,Order $order, OrderOffer $orderOffer){

        $user = Auth::user();

        if ($request->payment == 'online'){

            if ($order->is_pay == 0){

                $this->acceptedOffer($order,$orderOffer);

                $this->updateOrder($order);
            }
            $this->paymentOrder($user,$order,'online',$request,1);

        }else{

            $this->acceptedOffer($order,$orderOffer);

            $bank_transfer =  $this->createBankTransfer($user,$request,$order,$orderOffer);

            $this->paymentOrder($user,$order,'cash',$request,0, $bank_transfer);
        }
        return   $this->success('تم بنجاح التحويل');
    }

    function createBankTransfer($user , $request, $order, $orderOffer){

        $bankTransfer = new BankTransfer();
        $bankTransfer->user_id = $user->id;
        $bankTransfer->order_id = $order->id;
        $bankTransfer->bank_id  = $request->bankId;
        $bankTransfer->offer_id  = $orderOffer->id;
        $bankTransfer->image = $request->image;
        $bankTransfer->save();

        event(new NewBankTransfer($user, $order));

        return $bankTransfer;
    }

    function paymentOrder($user , $order , $pay_payment  , $request,$is_review,$bank_transfer = null){

        $orderOffer = $order->offers->where('order_id',$order->id)->first();
        $price = $order->total_price / $orderOffer->batches ;

        $payment                = new Payment();
        $payment->user_id       = $user->id;
        $payment->order_id      = $order->id;
        $payment->payment       = $pay_payment;
        $payment->price         = number_format($price,2);
        $payment->is_review     = $is_review;
        if ($bank_transfer){
            $payment->bank_transfer_id   = $bank_transfer->id;
        }

        if ($request->paymentId){
            $payment->paymentId  = $request->paymentId;
        }
        $payment->save();

       if ($pay_payment == 'online'){
           $this->updateNextDateTransfer($order,$orderOffer);

           $order->decrement('batches_remaining');
       }
    }

    function updateOrder($order){

        $order->update(['is_pay' => 1 ]);

        $conversation = new Conversation();
        $conversation->order_id = $order->id;
        $conversation->save();
        $conversation->users()->attach([Auth::id(), $order->provider_id]);
    }

    function acceptedOffer($order , $offer){

        $status =  $order->whereStatus('pending')->first();

        if ($status){

            $deleteOffer = $offer->where('order_id',$order->id)->where('provider_id' ,'!=' ,  $offer->provider_id )->get();

            if (count($deleteOffer) > 0){  $deleteOffer->each->delete();  }

            $deleteNotify = Notification::where('order_id',$order->id)->where('user_id' ,'!=' ,  $offer->provider_id )->get();

            if (count($deleteNotify) > 0){  $deleteNotify->each->delete();  }

            $order->update(['provider_id' =>  $offer->provider_id,'total_price' => $offer->price,
                'batches_remaining' => $offer->batches  ,'status' => 'accepted']);

        }


    }

    function updateNextDateTransfer($order , $offer){

        if ($offer){

            $masterOffer = $offer;
        }else{

            $masterOffer =  $order->offers->where('order_id',$order->id)->first();
        }
        $duration  = $masterOffer->duration  ;

        if ($duration !== 0){

            $next_transfer =  $order->next_transfer !== null ? Carbon::parse($order->next_transfer)->addDay($duration) : Carbon::today()->addDay($duration);

            $order->update(['next_transfer' => $next_transfer]);
        }
    }


}
