<?php

namespace App\Http\Controllers\Designer;

use App\Http\Helpers\Images;
use App\Http\Requests\Admin\updateWork;
use App\Models\PreviousWork;
use App\Models\PreviousWorkImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PreviousworksController extends Controller
{

    public $public_path;

    function __construct()
    {
        $this->public_path = 'files/';

    }


    public function index()
    {
        $previousworks =  PreviousWork::whereUserId(Auth::id())->get();

        return view('Provider.previousworks.index',compact('previousworks'));
    }


    public function create()
    {

        return view('Provider.previousworks.create');
    }


    public function store(updateWork $request)
    {
        $previousworks = new  PreviousWork();
        $previousworks->user_id = auth()->id();
        $previousworks->description  = $request->description_ar;
        if ($request->hasFile('master_image')):
            $previousworks->image =    $this->public_path . Images::uploadImage($request,'master_image',$this->public_path);
        endif;
        $previousworks->save();

        if ($request->hasFile('image')):

            $this->insertImagesWorking($request , $previousworks);

        endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => '  الأعمال']),
            "url" => route('previous-works.index'),

        ]);
    }


    public function edit($id)
    {

        $product = PreviousWork::with('images')->findOrFail($id);


        return view('Provider.previousworks.edit',compact('product'));
    }


    public function update(updateWork $request, $id)
    {
        $previousworks = PreviousWork::findOrFail($id);
        $previousworks->user_id = auth()->id();
        $previousworks->description  = $request->description_ar;
        if ($request->hasFile('master_image')):
            $previousworks->image =    $this->public_path . Images::uploadImage($request,'master_image',$this->public_path);
        endif;
        $previousworks->save();

        if ($request->hasFile('image')):
            $this->insertImagesWorking($request , $previousworks);
        endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => '  سابقة العمل']),
            "url" => route('previous-works.index'),
        ]);
    }

    function  insertImagesWorking($request , $previousworks){
        $data = [];

        foreach($request->file('image') as $key =>  $image )
        {
            $name  =  time() . '.' .Str::random(10) .$image->getClientOriginalName();
            Storage::disk('public')->put($name,  File::get($image));

            $data[$key]['previous_work_id'] = $previousworks->id;
            $data[$key]['image'] = $this->public_path .$name;
        }
        PreviousWorkImage::insert($data);
    }



    public function delete(Request $request)
    {
        $previousworks = PreviousWork::findOrFail($request->id);

        if ($previousworks->delete()) {
            return response()->json([
                'status' => true,
                'data' => $previousworks->id
            ]);
        }
    }


    public function deleteImage(Request $request)
    {
        $previousworks = PreviousWorkImage::findOrFail($request->id);

        if ($previousworks->delete()) {
            return response()->json([
                'status' => true,
                'data' => $previousworks->id
            ]);
        }
    }
}
