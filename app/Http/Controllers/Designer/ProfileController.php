<?php

namespace App\Http\Controllers\Designer;


use App\Http\Requests\Provider\EditProfile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProfileController extends Controller
{

    public function show($id)
    {
        $user = User::whereId($id)->first();

        return view('Provider.profile.show',compact('user'));

    }

    public function edit($id)
    {
        $user = User::whereId($id)->first();

        return view('Provider.profile.edit',compact('user'  ));
    }

    public function update(EditProfile $request, $id)
    {
        $user = Auth::user();
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->phone        = $request->phone;
        if ($request->bankAccountNumber):
            $user->bankAccountNumber        = $request->bankAccountNumber;
        endif;
        if ($request->hasFile('image')):
            $name= time() . '.' . Str::random(20) . urlencode($request->image->getClientOriginalName());
            $request->image->move(public_path().'/files/', $name);
            $user->image =   'files/'.$name;
        endif;

        if($request->has('password') && $request->password != null)
        {
            $user->password = $request->password;
        }

        $user->save();
        session()->flash('success', 'لقد تم تحديث  البيانات بنجاح.');
        return redirect()->back();
    }

    public function changePassword(Request $request){

        $user = \Auth::user();

        if ( Hash::check( $request->oldPassword , $user->password ) ) {

            $user->update( [ 'password' => $request->newPassword ] );

            return response()->json( [
                'status' => 200 ,
                'message' =>  trans('global.password_was_edited_successfully')  ,
            ] , 200 );

        }
        else {
            return response()->json( [
                'status' => 400 ,
                'error' => (array) trans('global.old_password_is_incorrect')  ,
            ] , 200 );
        }
    }

}
