<?php

namespace App\Http\Controllers\Designer;

use App\Models\Conversation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderOffer;
use Illuminate\Support\Facades\Auth;

class OrderProjectsController extends Controller
{

    public function index()
    {
//        $orders =  Order::whereHas('offers',function ($q){
//            $q->where('provider_id',auth()->id());
//        })->with('conversation','user')->get();
//
//        return  $orders;

        $orders = Auth::user()->order_provider()->whereHas('offers')->with('user','conversation')->get();


        return view('Provider.orders.projects.index',compact('orders'));
    }


    public function show($id)
    {
       $order = Order::with(['offers'=> function ($q) use ($id){
           $q->where('order_id', $id)->first();

       }])->findOrFail($id);

        if ($order['images']){

//            $filter =  explode(",",implode(",", $order['images']));
//            $order['images'] = $this->getAllImage($filter);
            $order['images'] = $this->getAllImage($order['images']);
        }

       $offer = $order['offers'][0];

        return view('Provider.orders.projects.details',compact('order','offer'));
    }

    function getAllImage($ids){
        if ($ids)
            return \App\Models\UploadImage::whereIn('id',$ids)->get(['image'])->pluck('image');

    }


    public function addOffer(Request $request)
    {

        $offer = OrderOffer::whereOrderId($request->id)->whereProviderId(Auth::id())->with('order')->first();

        if (!$offer){
            session()->flash('warning','نعتذر ولكن تبين ان المستخدم قبل عرض اخر ');
            return redirect()->back();
        }

        if ($offer['order']->time_out == 1){
            session()->flash('myErrors','نعتذر ولكن تبين ان تم إنتهاء الوقت المحدد من الإدارة ');
            return redirect()->back();
        }


        $offer->total_time  = $request->total_time;
        $offer->batches     = $request->batches;
        $offer->duration    = $request->duration;
        $offer->price       = $request->price;
        $offer->save();

        session()->flash('success','تم إرسال العرض بنجاح');
        return redirect()->back();
    }
}
