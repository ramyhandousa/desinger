<?php

namespace App\Http\Controllers\Designer;

use App\Libraries\PushNotification;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\OrderProduct;
use App\Models\Device;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductConversationsController extends Controller
{

    public $push;
    public function __construct(  PushNotification $push)
    {
        $this->push = $push;
    }


    public function index(Request $request)
    {
        $conversation  = Conversation::with('order.product.images')->findOrFail($request->convId);


        $product = $conversation['orderProduct']['product'];

        $filter = function ($q){
            $q->select('id','defined_user','name' , 'image');
        };
        $messages = Message::where('conversation_id', $request->convId)->with(['user' => $filter])->get();

        $messages->map(function ($q){
            $q->pathInfo =   pathinfo($q->fileName,PATHINFO_EXTENSION )  ;
        });
        return view('Provider.chat.index' ,compact('messages','product','conversation'));
    }


    public function projectChatting(Request $request){

        $order = Auth::user()->order_provider()->whereHas('conversation',function ($conversation) use ($request){
            $conversation->where('id',$request->convId);
        })->with('user','conversation.messages.user')->firstOrFail();

        $conversation = $order['conversation'];

        $messages = $order['conversation']['messages'];

        return view('Provider.chat.projectChat' ,compact('messages','order','conversation'));
    }


    public function store(Request $request)
    {
        $user = Auth::user();
        $conversation = Conversation::whereId($request->convId)->with('users.devices')->first();

        $sender = $conversation['users']->where('id','!=', $user->id)->first();

        $devices = Device::where('user_id', $sender->id)->pluck('device');

        $message = new Message();
        $message->message = $request->chat_message;
        $message->conversation_id = $conversation->id;
        $message->user_id = $user->id;
        $message->save();

        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, $user->name,$request->chat_message ,
                [
                    'id'                =>  $message->id,
                    'user_id'           =>  $user->id ,
                    'user_image'        =>  $user->imageProfile(),
                    'conversation_id'   =>  $conversation->id ,
                    'orderId'           =>  $conversation->order_id ,
                    'message'           =>  $request->chat_message ,
                ]
            );
        }
    }


}
