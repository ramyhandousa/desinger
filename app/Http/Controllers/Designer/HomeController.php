<?php

namespace App\Http\Controllers\Designer;


use App\Models\Order;
use \Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class HomeController extends Controller
{




    public function __construct()
    {


    }

    public function index()
    {

         if (!auth()->check())
            return redirect(route('admin.login'));

         $order_projects = Order::whereProviderId(Auth::id());

        $order_projects_pending = $order_projects->whereStatus('pending')->count();
        $order_projects_finish = $order_projects->whereStatus('finish')->count();

        return view('Provider.home.index',
            compact( 'order_projects_pending',
                            'order_projects_finish'));
    }

    public function information_admin () {



        return view('Provider.staticData');
    }

}
