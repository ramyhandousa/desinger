<?php

namespace App\Http\Controllers\Designer;

use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Conversation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrderProduct;

class OrderProductsController extends Controller
{

    public $notify;
    public $push;

    public function __construct( InsertNotification $notification,PushNotification $push)
    {

        $this->notify = $notification;
        $this->push = $push;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = OrderProduct::where('provider_id',auth()->id())->get();

        $orders->map(function ($order){
           $order->convId = Conversation::where('product_order_id',$order->id)->first() ;
        });

         return view('Provider.orders.products.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = OrderProduct::findOrFail($id);

        return view('Provider.orders.products.details',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function acceptOrder(Request $request)
    {
        $product_order  = OrderProduct::with('user','provider')->findOrFail($request->id);
          if($product_order) $product_order->status = 'accepted';
         $product_order->save();
        $this->notify->NotificationDbType(4,$product_order['user'],$product_order['provider'],
                                                    $product_order->product_id,$product_order);

        return response()->json([
            'status'=>true,
            'title'=>"نجاح",
            'message'=>"تم قبول الطلب بنجاح",
        ]);
    }

    public function refuseOrder(Request $request)
    {

         $product_order  = OrderProduct::with('user','provider')->findOrFail($request->id);


         if (Carbon::now()->format('Y-m-d') >  $product_order->created_at->format('Y-m-d') ){
             session()->flash('warning','للاسف تاريخ الطلب اكبر من التاريخ الحالي ');
             return redirect()->back();
         }

         if($product_order)
         {
            $product_order->refuse_reason = $request->refuse_reason;
            $product_order->status= 'refuse';
            $product_order->save();
         }
        $this->notify->NotificationDbType(5,$product_order['user'],$product_order['provider'], $request->refuse_reason,$product_order);
        session()->flash('success','تم رفض الطلب بنجاح');
        return redirect()->back();

    }
}
