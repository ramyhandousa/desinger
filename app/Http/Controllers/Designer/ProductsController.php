<?php

namespace App\Http\Controllers\Designer;

use App\Http\Requests\Provider\storeProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductImage;
use UploadImage;
use Validator;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
     public $public_path;

    function __construct()
    {
        $this->public_path = 'files/products/';

    }

    public function index()
    {
        $products  =auth()->user()->products;

        return view('Provider.products.index',compact('products'));
    }

    public function create()
    {
        $categories = auth()->user()->categories()->get();
        return view('Provider.products.create',compact('categories'));
    }

    public function store(Request $request)
    {
        //Get input ...
        $post_data = [
            'image.*' => $request->image,
        ];

        //set Rules .....
        $valRules = [
            'image.*' => 'image|mimes:jpeg,png,jpg|max:1000'
        ];

        //Declare Validation message
        $valMessages = [
            'image' => 'يجب إضافة صورة لإضافة مدير وتكون بصيغة PNG او JPG',
            'image.max' => '  اقصي حجم مسموح به هو 1 ميجا للصورة   ',
        ];

        $valResult = Validator::make($post_data, $valRules, $valMessages);
        if ($valResult->passes()) {
        $product = new  Product();
        $product->category_id = $request->category_id;
        $product->user_id = auth()->id();
        $product->{'name:ar'} = $request->name_ar;
        $product->{'name:en'} = $request->name_en;
        $product->{'description:ar'} = $request->description_ar;
        $product->{'description:en'} = $request->description_en;
        $product->price = $request->price;
        $product->save();

       if ($request->hasFile('image')):
            foreach($request->file('image') as $image)
            {
                $name=  time() . '.' . str_random(20) .$image->getClientOriginalName();
                $image->move(public_path().'/files/products/', $name);
                $images = new  ProductImage();
                $images->product_id = $product->id;
                $images->url =  '/public/' . $this->public_path .$name;
                $images->save();
            }
       endif;

       if ($request->hasFile('files')):
            foreach($request->file('files') as $image)
            {
                $name=  time() . '.' . str_random(20) .$image->getClientOriginalName();
                $image->move(public_path().'/files/products/', $name);
                $images = new  ProductImage();
                $images->product_id = $product->id;
                $images->url =  '/public/' . $this->public_path .$name;
                $images->paid =  0;
                $images->save();
            }
       endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => '  المنتج']),
            "url" => route('products.index'),
        ]);
        } else {
            // Grab Messages From Validator
            $valErrors = $valResult->errors()->first();
            return response()->json([
                'status' => false,
                "message" => $valErrors,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        $categories = auth()->user()->categories()->get();
        return view('Provider.products.edit',compact('product','categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(storeProduct $request, $id)
    {

        $product = Product::findOrFail($id);
        $product->category_id = $request->category_id;
        $product->{'name:ar'} = $request->name_ar;
        $product->{'name:en'} = $request->name_en;
        $product->{'description:ar'} = $request->description_ar;
        $product->{'description:en'} = $request->description_en;
        $product->price = $request->price;
        $product->save();

           if ($request->hasFile('image') ):
                foreach($request->file('image') as  $key => $image )
                {
                    $images =   ProductImage::whereId($key)->first();
                    if (!$images) {   $images =  new ProductImage();  }
                    $name=  time() . '.' . str_random(20) .$image->getClientOriginalName();
                    $image->move(public_path().'/files/products/', $name);
                    $images->product_id = $product->id;
                    $images->url =  '/public/' . $this->public_path .$name;
                    $images->save();
                }

           endif;

        if ($request->hasFile('files')):
            foreach($request->file('files') as  $key => $image )
            {
                $images =   ProductImage::whereId($key)->first();
                if (!$images) {   $images =  new ProductImage();  }
                $name=  time() . '.' . str_random(20) .$image->getClientOriginalName();
                $image->move(public_path().'/files/products/', $name);
                $images->product_id = $product->id;
                $images->url =  '/public/' . $this->public_path .$name;
                $images->paid =  0;
                $images->save();
            }
        endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => '  المنتج ']),
            "url" => route('products.index'),

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function suspend(Request $request)
    {
        $model = Product::findOrFail($request->id);

        $waiting = $model->orders()->where(function($q){
                            $q->where('status','pending')->orWhere('status','accepted');
                        })->count() > 0 ;

        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            if($waiting){

                return response()->json([
                    'status' => false,
                    'message' => "لا يمكن حظر المنتج لوجود  طلبات جاريه به",

                ]);

            }
            $message = "لقد تم الحظر على مستوي النظام بنجاح";

        } else {
            $message = "لقد تم فك الحظر  بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }

    public function deleteImage(Request $request)
    {
        $previousworks = ProductImage::findOrFail($request->id);

        if ($previousworks->delete()) {
            return response()->json([
                'status' => true,
                'data' => $previousworks->id
            ]);
        }
    }

}
