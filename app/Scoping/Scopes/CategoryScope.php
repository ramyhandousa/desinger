<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 4/10/2020
 * Time: 9:11 AM
 */

namespace App\Scoping\Scopes;
use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class CategoryScope implements Scope
{

    public function apply(Builder $builder , $value){

        return $builder->whereHas('category',function ($builder) use ($value){

            $builder->where('category_id',$value)->where('is_suspend',0);
        });
    }

}
