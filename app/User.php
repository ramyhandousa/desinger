<?php

namespace App;

use App\Models\Conversation;
use App\Models\Device;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Profile;
use App\Models\VerifyUser;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable
{
    use Notifiable ,HasRolesAndAbilities;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'defined_user','code_country', 'name', 'email', 'phone', 'password','is_payed', 'image',
        'is_active', 'is_accepted','is_suspend','message' ,'api_token'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasAnyRoles()
    {
        if (auth()->check()) {

            if (auth()->user()->roles->count()) {
                return true;
            }

        } else {
            redirect(route('admin.login'));
        }
    }

    public function imageProfile(){
        $image = $this->attributes['image'];
        if ($image){
                return URL('/') .'/'. $image;
        }
        return URL('/') . '/default.png';
    }

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function devices(){
        return $this->hasMany(Device::class);
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function code(){
        return $this->hasOne(VerifyUser::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function order_provider(){
        return $this->hasMany(Order::class,'provider_id');
    }


    public function notifications(){
        return $this->hasMany(Notification::class);
    }


    public function rating(){
        return $this->belongsToMany(User::class,'user_rate','provider_id','user_id')->withPivot('rate','order_id','message');
    }

    public function conversations()
    {
        return $this->belongsToMany(Conversation::class);
    }

}
