<?php

namespace App\Listeners;

use App\Events\NewProjectNotify;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewProjectNotifyListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  NewProjectNotify  $event
     * @return void
     */
    public function handle(NewProjectNotify $event)
    {

        $devices = Device::where('user_id', $event->provider->id)->pluck('device');

        $notify =   $this->notify->NotificationDbType(6,$event->provider,$event->user, $event->request,$event->order);

        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, $notify['title'], $notify['body'],
                [
                    'id'            => $notify['id'],
                    'orderId'       => $notify['order_id'],
                    'href'          => \request()->root(). '/ar/Dashboard/orders-projects/' . $notify['order_id'],
                    'type'          => $notify['type'],
                    'is_read'       => $notify['is_read'],
                    'title'         => $notify['title'],
                    'body'          => $notify['body'],
                    'created_at'    => $notify['created_at'],
                ]
            );
        }
    }
}
