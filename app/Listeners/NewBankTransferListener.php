<?php

namespace App\Listeners;

use App\Events\NewBankTransfer;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NewBankTransferListener
{


    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  NewBankTransfer  $event
     * @return void
     */
    public function handle(NewBankTransfer $event)
    {
        $devices = Device::where('user_id', 1 )->pluck('device');

        $notify =    $this->notify->NotificationDbType(20,$event->user,null,null,$event->order);

        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, $notify['title'], $notify['body'],
                [
                    'id'            => $notify['id'],
                    'type'          => $notify['type'],
                    'is_read'       => $notify['is_read'],
                    'title'         => $notify['title'],
                    'body'          => $notify['body'],
                    'href'          => route('bank-transfer-admin'),
                    'created_at'    => $notify['created_at'],
                ]
            );
        }
    }
}
