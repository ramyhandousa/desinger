<?php

namespace App\Providers;

use App\Events\AcceptAdminPaymentNotify;
use App\Events\ChattingUserNotify;
use App\Events\ContactUs;
use App\Events\NewBankTransfer;
use App\Events\RefuseAdminPaymentNotify;
use App\Events\SubscriptionPayment;
use App\Listeners\AcceptAdminPaymentNotifyListener;
use App\Listeners\ChattingUserNotifyListener;
use App\Listeners\ContactUsListener;
use App\Listeners\NewBankTransferListener;
use App\Listeners\RefuseAdminPaymentNotifyListener;
use App\Listeners\SubscriptionPaymentListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        'App\Events\PhoneOrEmailChange' => [
            'App\Listeners\PhoneOrEmailChangeListener',
        ],
        'App\Events\UserLogOut' => [
            'App\Listeners\UserLogOutListener',
        ],
        'App\Events\NewProjectNotify' => [
            'App\Listeners\NewProjectNotifyListener',
        ],
        'App\Events\NewOrderOffer' => [
            'App\Listeners\NewOrderOfferListener',
        ],

        NewBankTransfer::class => [
            NewBankTransferListener::class,
        ],
        ContactUs::class => [
            ContactUsListener::class,
        ],
        SubscriptionPayment::class => [
            SubscriptionPaymentListener::class,
        ],
        AcceptAdminPaymentNotify::class => [AcceptAdminPaymentNotifyListener::class],
        RefuseAdminPaymentNotify::class => [RefuseAdminPaymentNotifyListener::class],
        ChattingUserNotify::class       => [ChattingUserNotifyListener::class],


    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
