<?php

namespace App\Providers;

use App\Models\Notification;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {

            $helper = new \App\Http\Helpers\Images();
            $setting = new Setting();
//            $usersCount = User::where('defined_user','user')->doesntHave('profile')->count();
//            $dealerCount = User::where('defined_user','designer')->count();
//            $admin_notification_system = Notification::whereUserId(Auth::id())->latest()->get();
            $notification_system = Auth::check() ?  Auth::user()->notifications : [];


            $view->with(compact('helper','setting','notification_system'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}


