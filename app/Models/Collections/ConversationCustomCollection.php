<?php


namespace App\Models\Collections;


use App\Http\Resources\User\UserImageResource;
use Illuminate\Database\Eloquent\Collection;

class ConversationCustomCollection extends Collection
{

    public function indexData(){

        return  $this->map(function ($conversation){
            $sender = $conversation->users()->where('id','!=',auth()->id())->first();
            return [
                'id'            =>  $conversation->id,
                'message'       =>   $this->getLastMessage($conversation,$sender),
                'user'          =>   new UserImageResource($sender) ,
//                'order_id'      =>  $conversation->order_id,
                'open'        =>  $conversation->status == 'open' ,
                'created_at'     =>   $this->getArabicMonth( $conversation->created_at),
            ];
        })->reverse()->values();
    }

    function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $year = date("Y", strtotime($data));

        $month = $months[$month];

        return $day . ' ' . $month ;
    }

    function getLastMessage($conversation , $sender){
        if ( count($conversation->last_messages) > 0 ){

            $message = $conversation->last_messages->where('user_id',$sender->id);

            return  collect($message)->reverse()->values()->first()['message'];

        }else{
            return  $sender->name .'  يمكنك بدا المحادثة الأن مع ' ;
        }
    }
}
