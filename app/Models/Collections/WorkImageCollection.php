<?php


namespace App\Models\Collections;


use Illuminate\Database\Eloquent\Collection;

class WorkImageCollection extends Collection
{

    public function showData(){
        return  $this->map(function ($work_image){
            return [
                'id'            =>  $work_image->id,
                'image'         =>  $work_image->image
            ];
        });
    }
}
