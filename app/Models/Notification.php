<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['topic','order_type','user_id','sender_id','order_id','product_id',
                            'offer_id','title','body','type','is_read'];

    public function user()
    {

        return $this->belongsTo(User::class);
    }

    public function orderProduct()
    {
        return $this->belongsTo(OrderProduct::class, 'order_id');
    }


    public function orderProject()
    {
        return $this->belongsTo(OrderProject::class, 'order_id');
    }


}
