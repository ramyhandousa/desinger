<?php

namespace App\Models;

use App\Models\Collections\WorkImageCollection;
use Illuminate\Database\Eloquent\Model;

class PreviousWorkImage extends Model
{
    public $timestamps = false;


    public function newCollection(array $models = [])
    {
        return new WorkImageCollection($models);
    }
}
