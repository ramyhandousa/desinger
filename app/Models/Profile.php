<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id' , 'experience_year' ,'cv' , 'images_work' ,'desc'
    ];


    protected $casts = [
        'images_work' => 'array'
    ];


}
