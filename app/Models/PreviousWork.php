<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreviousWork extends Model
{


    public function images(){
        return $this->hasMany(PreviousWorkImage::class,'previous_work_id');
    }
}
