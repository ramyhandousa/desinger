<?php

use App\Mail\mailActiveAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Api',
    'prefix' => 'v1'
], function () {

    Route::group(['prefix' => 'Auth'], function () {

        Route::post('login','AuthController@login');
        Route::post('sign-up', 'AuthController@register');
        Route::post('sign-up-engineer', 'AuthController@registerEngineer');
        Route::post('forgetPassword', 'AuthController@forgetPassword');
        Route::post('resetPassword', 'AuthController@resetPassword');
        Route::post('checkCode', 'AuthController@checkCodeActivation');
        Route::post('resendCode', 'AuthController@resendCode');
        Route::post('changPassword', 'AuthController@changPassword');
        Route::post('editProfile', 'AuthController@editProfile');
        Route::post('editEmail', 'AuthController@editEmail');
        Route::post('editPhone', 'AuthController@editPhone');
        Route::post('logOut','AuthController@logOut');
        Route::post('upload-image','AuthController@upload');
        Route::post('remove-image','AuthController@removeImage');
        Route::post('remove-user','AuthController@removeUser');
        Route::post('remove-engineer','AuthController@removeEngineer');

    });

    Route::group(['prefix' => 'homeScreen'], function () {

        Route::get('/','HomeController@index');
        Route::get('/{previousWork}','HomeController@show');
    });

    Route::group(['prefix' => 'users'], function () {

        Route::get('show-user/{id}','UserController@show_user');
    });

    Route::resource('orders','OrderController');
    Route::post('orders/rate/{order}','OrderController@rateOrder');
    Route::get('orders/offer/list/{order}','OfferController@index');
    Route::post('orders/offer/{orderOffer}','OfferController@acceptedOffer');
    Route::post('orders/payment/{order}/{orderOffer?}','PaymentController@payOrder');


    Route::resource('conversation','MessageController');
    Route::post('conversation/complete/{conversation}','MessageController@complete');

    Route::group(['prefix' => 'setting'], function () {

        Route::get('banks','SettingController@banks');
        Route::get('aboutUs','SettingController@aboutUs');
        Route::get('terms','SettingController@terms_user');
        Route::get('typesSupport','SettingController@getTypesSupport');
        Route::post('contact_us','SettingController@contact_us');
    });

    Route::post('test_queue',function (){
        $user = \App\User::find(26);
        $action_code = substr(rand(), 0, 4);
//        Mail::to($user->email)->send(new mailActiveAccount($user, $action_code));
//       \App\Jobs\testQueue::dispatchAfterResponse();
        \App\Jobs\ProcessMailSend::dispatch($user ,$action_code , mailActiveAccount::class)->delay(now()->addMinutes(1));

//        \App\Jobs\testQueue::dispatch($user, );
       return $user;
    });
});

