<?php
use Illuminate\Support\Facades\Route;

Auth::routes();



Route::get('/', 'HomeController@index')->name('home');

//Route::get('lang/{language}', 'LanguageController@switchLang')->name('lang.switch');

Route::group(['prefix' => 'Dashboard' ], function () {

    Route::get('/login', 'LoginController@login')->name('Provider.login');
    Route::post('/login', 'LoginController@postLogin')->name('Provider.postLogin');
    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('Provider.password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('Provider.password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('Provider.password.reset.token');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});

Route::group(['prefix' => 'Dashboard', 'middleware' => ['admin']], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('Provider.home');
    Route::resource('products','ProductsController');
    Route::post('products/suspend', 'ProductsController@suspend')->name('products.suspend');
    Route::post('products/deleteImage', 'ProductsController@deleteImage')->name('products.deleteImage');

    Route::resource('/orders-products','OrderProductsController');

    Route::resource('/previous-works','PreviousworksController');
    Route::post('previous-works/delete', 'PreviousworksController@delete')->name('previous-works.delete');
    Route::post('previous-works/deleteImage', 'PreviousworksController@deleteImage')->name('previous-works.deleteImage');

    Route::post('/orders-products/accept','OrderProductsController@acceptOrder')->name('orders-products.accept');
    Route::post('/orders-products/refuse','OrderProductsController@refuseOrder')->name('orders-products.refuse');
    Route::resource('/orders-projects','OrderProjectsController');
    Route::post('/orders-projects/offer','OrderProjectsController@addOffer')->name('orders-projects.addoffer');

    Route::get('information_admin','HomeController@information_admin')->name('information_admin');

    Route::resource('gym_profile', 'ProfileController');

    Route::post('changeMyPassword' ,'ProfileController@changePassword')->name('Provider.changePassword');

    Route::resource('product-conversations-orders', 'ProductConversationsController');
    Route::get('projectChatting', 'ProductConversationsController@projectChatting')->name('projectChatting');

    Route::resource('contact_us_inbox', 'ContactUsController');
    Route::get('updateIsRead', 'ContactUsController@updateIsRead')->name('Provider.support.updateIsRead');
    Route::get('updateIsDeleted', 'ContactUsController@updateIsDeleted')->name('Provider.support.updateIsDeleted');
    Route::get('removeAllMessages', 'ContactUsController@removeAllMessages')->name('Provider.support.removeAllMessages');

    Route::get('/myWallet', 'ProfileController@myWallet')->name('Provider.myWallet');

    Route::post('/provider_logout', 'LoginController@logout')->name('Provider.logout');
});
