@extends('Provider.layouts.master')
@section('title', 'إدارة التقارير ')
@section('content')

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">عدد الحجوزات المكتملة </h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <div style="display:inline;width:80px;height:80px;"><canvas width="80" height="80px"></canvas>
                                <input data-plugin="knob" data-width="80" data-height="80" data-fgcolor="#10c469"
                                       data-bgcolor="#AAE2C6" value="{{$orderComplete}}" data-skin="tron" data-angleoffset="180"
                                       data-readonly="true" data-thickness=".15" readonly="readonly"
                                       style="width: 44px; height: 26px; position: absolute; vertical-align: middle;
                                       margin-top: 26px; margin-right: -62px; border: 0px;
                                       background: none; font: bold 16px Arial; text-align: center; color: rgb(16, 196, 105); padding: 0px; -webkit-appearance: none;"></div>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0" data-plugin="counterup">{{$orderComplete}}</h2>
                            <p class="text-muted">حجز</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">  المبلغ المستحق لك  من الأدمن</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <div class="widget-user">
                                <img src="{{Url('/') .'/public/wallet.png'}}" class="img-responsive img-circle" alt="user">
                            </div>

                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0" data-plugin="counterup">{{$walletOrders_gym - $my_wallet}}</h2>
                            <p class="text-muted">رصيد</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">الرصيد الحالي لك  </h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <div class="widget-user">
                                <img src="{{Url('/') .'/public/wallet.png'}}" class="img-responsive img-circle" alt="user">
                            </div>

                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0" data-plugin="counterup">{{$my_wallet}}</h2>
                            <p class="text-muted">رصيد</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
