
@extends('Provider.layouts.master')
@section('title', 'تعديل  بياناتي ')
@section('content')


    <form method="POST" action="{{ route('gym_profile.update', Auth::id()) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}



    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">

                    <a href="#" onclick="window.history.back();return false;"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>
                </div>
                <h4 class="page-title">تعديل بياناتي</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30">تعديل  بياناتي</h4>


                    <div class="col-xs-12">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="userName">الاسم الكامل*</label>
                            <input type="text" name="name" value="{{ $user->name   }}" class="form-control"
                                   required
                                   placeholder="اسم المستخدم بالكامل..."
                                   data-parsley-maxLength="50"
                                   data-parsley-maxLength-message=" الاسم  يجب أن يكون 50 حروف فقط"
                                   data-parsley-minLength="3"
                                   data-parsley-minLength-message=" الاسم  يجب أن يكون اكثر من 3 حروف "
                                   data-parsley-required-message="يجب ادخال  اسم المستخدم"

                            />
                            <p class="help-block" id="error_userName"></p>
                            @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                            @endif
                        </div>
                    </div>



                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="userPhone">رقم الجوال*</label>
                            <input type="text" name="phone" value="{{ $user->phone   }}"
                                   class="form-control"
                                   required
                                   data-parsley-maxLength="10"
                                   data-parsley-maxLength-message=" رقم الجوال  يجب أن يكون 10 حروف فقط"
                                   data-parsley-minLength="5"
                                   data-parsley-minLength-message=" رقم الجوال  يجب أن يكون اكثر من 5 حروف "
                                   data-parsley-required-message="يجب ادخال رقم الجوال"
                                   placeholder="رقم الجوال..."/>
                            @if($errors->has('phone'))
                                <p class="help-block">
                                    {{ $errors->first('phone') }}
                                </p>
                            @endif
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="emailAddress">البريد الإلكتروني*</label>

                            <input type="email" name="email" parsley-trigger="change"
                                   value="{{ $user->email  }}"
                                   class="form-control"
                                   placeholder="البريد الإلكتروني..."
                                   data-parsley-type="email"
                                   data-parsley-type-message="أدخل البريد الالكتروني بطريقة صحيحة"
                                   data-parsley-required-message="يجب ادخال  البريد الالكتروني"
                                   data-parsley-maxLength="30"
                                   data-parsley-maxLength-message=" البريد الالكتروني  يجب أن يكون ثلاثون حرف فقط"
                                   {{--data-parsley-pattern="/^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/gm"--}}
                                   {{--data-parsley-pattern-message="أدخل  البريد الالكتروني بطريقة الايميل ومن غير مسافات"--}}
                                   required
                            />
                            @if($errors->has('email'))
                                <p class="help-block">{{ $errors->first('email') }}</p>
                            @endif

                        </div>

                    </div>


{{--                    <div class="col-xs-12">--}}
{{--                        <div class="form-group{{ $errors->has('bankAccountNumber') ? ' has-error' : '' }}">--}}
{{--                            <label for="emailAddress">   الحساب البنكي*</label>--}}
{{--                            <input type="number" name="bankAccountNumber"--}}
{{--                                   value="{{ $user->bankAccountNumber  }}"--}}
{{--                                   oninput="validity.valid||(value='');"--}}
{{--                                   class="form-control"--}}
{{--                                   placeholder="   الحساب البنكي..."--}}
{{--                                   data-parsley-required-message="يجب ادخال   الحساب البنكي  "--}}
{{--                                   data-parsley-maxLength="40"--}}
{{--                                   data-parsley-maxLength-message="  الحساب البنكي    يجب أن يكون ثلاثون حرف فقط"--}}
{{--                                   required--}}
{{--                            />--}}
{{--                            @if($errors->has('bankAccountNumber'))--}}
{{--                                <p class="help-block">{{ $errors->first('bankAccountNumber') }}</p>--}}
{{--                            @endif--}}

{{--                        </div>--}}

{{--                    </div>--}}



                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            حفظ البيانات
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            إلغاء
                        </button>
                    </div>

                </div>
            </div><!-- end col -->

            <div class="col-lg-4">
                <div class="card-box" style="overflow: hidden;">


                    <h4 class="header-title m-t-0 m-b-30">الصورة الشخصية</h4>

                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                        <div class="col-sm-12">

                            <input type="hidden" value="{{  $user->imageProfile() }}" name="oldImage"/>
                            <input type="file" name="image" class="dropify" data-max-file-size="6M" data-show-remove="false"
                                   data-default-file="{{  $user->imageProfile() }}"/>

                        </div>
                        @if($errors->has('image'))
                            <p class="help-block">{{ $errors->first('image') }}</p>
                        @endif
                    </div>

                </div>
            </div><!-- end col -->
        </div>
        <!-- end row -->
    </form>

@endsection


@section('scripts')
    <script type="text/javascript">

        $(document).ready(function() {
            $('label').bind('click',function(){
                var input = $(this).find('input').control();

                console.log(input)
//                if(input.prop('checked')){
//                    input.prop('checked',false);
//                }else{
//                    input.prop('checked',true);
//                }
            });

            $('#myCategoriesClick').one('click', function() {
//                console.log(this.children)

//                swal({
//                    title: "سوف يتم إيقاف  طلب المنتجات لديك إذا ؟",
//                    text: "إذا قمت بتغير او إزالة التخصص لديك التابع لهذا التخصص  ",
//                    type: "warning",
////                        showCancelButton: true,
//                    confirmButtonColor: "#DD6B55",
//                    confirmButtonText: "موافق",
//                    confirmButtonClass: 'btn-success waves-effect waves-light',
//                    closeOnConfirm: true,
////                        closeOnCancel: true,
//                });
            });

        });
    </script>


@endsection
