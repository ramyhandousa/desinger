
@extends('Provider.layouts.master')
@section('title', 'الصفحة الرئيسية')

@section('styles')
    <style>

        .style-input{
            margin-top: 20px ;
        }
        </style>

@endsection

@section('content')


<div class="container">
    <div class="row">

        <div class="col-lg-12">
            <div class="panel panel-color panel-tabs panel-success">
                <div class="panel-heading">
                    <ul class="nav nav-pills pull-right">
                        <li class="active">
                            <a href="#navpills-1"  onclick="pauseVid()"  data-toggle="tab" aria-expanded="true">  الشروط والأحكام</a>
                        </li>
                        <li class="">
                            <a href="#navpills-2" onclick="pauseVid()"  data-toggle="tab" aria-expanded="false">  عن التطبيق  </a>
                        </li>
                        <li class="">
                            <a href="#navpills-3" onclick="pauseVid()"  data-toggle="tab" aria-expanded="false">  روابط التواصل  </a>
                        </li>

                    </ul>
                    <h3 class="panel-title">المعلومات  الأساسية عن التطبيق </h3>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="navpills-1"  class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        {{ $setting->getBody('terms_user_ar' ) }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="navpills-2"  class="tab-pane fade ">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        {{ $setting->getBody('about_us_ar' ) }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="navpills-3"   class="tab-pane fade">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box table-responsive m-t-0">

                                        <div class="form-group">

                                            <div class="col-lg-5 col-xs-12">
                                                <label>@lang('maincp.unified_number') </label>
                                                <input class="form-control" type="text" name="phone_contact"
                                                       value="{{ $setting->getBody('phone_contact') }}" placeholder="0123456789"
                                                       maxlength="500" >
                                            </div>

                                            <div class="col-lg-5 col-xs-12">
                                                <label>@lang('maincp.e_mail') </label>
                                                <input class="form-control" type="email" name="contactus_email"
                                                       value="{{ $setting->getBody('contactus_email') }}" placeholder="Example@Advertisement.sa"
                                                       maxlength="500"
                                                >
                                            </div>

                                            <div class="col-lg-5 col-xs-12">
                                                <div class="input-group customeStyleSocail">
                                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-facebook"></i></span>
                                                    <input type="text" class="form-control" name="faceBook"
                                                           value="{{ $setting->getBody('faceBook') }}"
                                                           placeholder="@lang('maincp.facebook') "
                                                           aria-label="Recipient's username" aria- describedby="basic-addon2"
                                                           maxlength="500" >
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-xs-12">
                                                <div class="input-group customeStyleSocail">
                                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-twitter"></i></span>
                                                    <input type="text" name="twitter"
                                                           value="{{ $setting->getBody('twitter') }}" class="form-control"
                                                           placeholder="@lang('maincp.twitter') "
                                                           aria-label="Recipient's username" aria- describedby="basic-addon2"
                                                           maxlength="500" >
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-xs-12">
                                                <div class="input-group customeStyleSocail">
                                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-whatsapp" aria-hidden="true"></i></span>
                                                    <input type="text" name="whatsapp"
                                                           value="{{ $setting->getBody('whatsapp') }}" class="form-control"
                                                           placeholder=" "
                                                           aria-label="Recipient's username" aria- describedby="basic-addon2"
                                                           maxlength="500" >
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-xs-12">
                                                <div class="input-group customeStyleSocail">
                                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-telegram" aria-hidden="true"></i></span>
                                                    <input type="text" name="telegram"
                                                           value="{{ $setting->getBody('telegram') }}" class="form-control"
                                                           placeholder=" "
                                                           aria-label="Recipient's username" aria- describedby="basic-addon2"
                                                           maxlength="500" >
                                                </div>
                                            </div>


                                            <div class="col-lg-5 col-xs-12">
                                                <div class="input-group customeStyleSocail">
                                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-snapchat"></i></span>
                                                    <input type="text" name="snapChat"
                                                           value="{{ $setting->getBody('snapChat') }}" class="form-control"
                                                           placeholder=" "
                                                           aria-label="Recipient's username" aria- describedby="basic-addon2"
                                                           maxlength="500">
                                                </div>
                                            </div>




                                            <div class="col-lg-5 col-xs-12">
                                                <div class="input-group customeStyleSocail">
                                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-instagram"></i></span>
                                                    <input type="text" name="instagram"
                                                           value="{{ $setting->getBody('instagram') }}" class="form-control"
                                                           placeholder="@lang('maincp.instagram')  "
                                                           aria-label="Recipient's username" aria- describedby="basic-addon2"
                                                           maxlength="500">
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <!-- end col -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('scripts')
    <script type="text/javascript">

        var videoplayer = document.getElementById("videoPlayer");

        function pauseVid() {
            videoplayer.pause();
        }


    </script>


@endsection
