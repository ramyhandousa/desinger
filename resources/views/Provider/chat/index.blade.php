@extends('Provider.layouts.master')
@section('title',__('maincp.personal_page'))

@section('styles')

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    <style>

        .msg-block {
            border-bottom: 2px solid #eee;
            padding-top: 20px;
        }

        .msg p {
            font-weight: 700;
            word-break: break-word;
        }

        .product-img {
            width: 100px;
            margin: auto;
            background: #eee;
            height: 100px;
            border-radius: 50%;
            margin-top: 50px;
            position: relative;
            border: 3px solid #09b5a8;
            /* box-shadow: 0 0 5px 0px rgb(9, 181, 168), 0 0px 15px 5px rgb(9, 181, 168); */
        }

        .product-img img {
            max-height: 100%;
            max-width: 100%;
            width: auto;
            height: auto;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
        }

        .product-name {
            padding-top: 15px;
        }

        .provider-name {
            color: #909090;
            padding-bottom: 15px;
            margin: 0;
        }

        .product-price {
            color: #09b5a8;
            font-size: 20px;
            font-weight: 700;
            padding-bottom: 15px;
            display: block;
        }

        .desc {
            font-weight: 700;
            line-height: 2;
            padding: 0px 30px;
        }

        .time {
            color: #969696;
            font-weight: 700;
        }

        .details {
            background-color: #09b5a8;
            height: 100%;
        }

        .msg-block {
            border-bottom: 2px solid #eee;
            padding-top: 20px;
        }

        .d-flex {
            display: flex
        }

        .msg {
            padding: 40px 60px;
        }

        .user-img {
            width: 50px;
            height: 50px;
            border-radius: 50%;
            border: 1px solid #eee;
            padding: 3px;
            background: #eee;
            margin: 0 20px;
        }

        .username {
            font-weight: 700;
            margin: 0;
        }

        .bg-white {
            background-color: #fff;
        }

        .msg-block:last-child {
            border-bottom: none;
            margin-bottom: 80px;
        }

        .cp-chat .card-box {
            height: 100%;
            min-height: 75vh;
            position: relative;
            overflow: hidden;
        }

        .msg_field {
            width: 100%;
            background: rgba(9, 181, 168, 1);
            position: absolute;
            bottom: 0;
            right: 0;
        }

        .msg_field textarea {
            width: 100%;
            background: transparent;
            color: #fff;
            padding: 15px 25px;
            border: none;
            padding-left: 88px;
            padding-bottom: 0;
        }

        .msg_field textarea::placeholder {
            color: #e0e0e0;
            font-size: 20px;
        }

        .msg_field #msg_send {
            position: absolute;
            left: 35px;
            color: #fff;
            transform: rotate(-88deg);
            font-size: 25px;
            margin-top: 13px;
            cursor: pointer;
        }

        .exit {
            color: #fff;
            opacity: 1;
            padding: 5px 9px;
            background: #09b5a8;
            display: block;
            margin-bottom: 15px;
        }

        .exit:hover {
            color: #09B5A8;
            background: #fff;
            opacity: 1;
        }

        #chat_converse {
            min-height: 75vh;
            max-height: 75vh;
            overflow-y: scroll;
        }
        /* width */

        ::-webkit-scrollbar {
            width: 10px;
            display: none;
        }
        /* Track */

        ::-webkit-scrollbar-track {
            background: #f1f1f1;
            display: none;
        }
        /* Handle */

        ::-webkit-scrollbar-thumb {
            background: #888;
            display: none;
        }
        /* Handle on hover */

        ::-webkit-scrollbar-thumb:hover {
            background: #555;
            display: none;
        }
        .cp-chat .card-box {
            height: 100%;
            min-height: 75vh;
            position: relative;
            overflow: hidden;
            padding-bottom: 73px;
        }
        .msg_field textarea{resize: none}
        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }

    </style>



@endsection

@section('content')



    <div class="">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right m-t-15">
                        <a href="{{URL::previous()}}"  class="exit">
                            رجوع
                        </a>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-sm-8 cp-chat">
                    <div class="card-box">
                        <div id="chat_converse" class="chat_converse">
                        @if(count($messages) > 0 )
                            @foreach($messages as $message)
                                <!-- user msg -->
                                    <div class="msg-block">
                                        <div class="d-flex" @if($message->user->defined_user == 'user') style="float: left" @endif>
                                            <div class="d-inline-block">
                                                <img src="{{$message->user->image ? request()->root().$message->user->image : request()->root().'/public/def_user.png'}}" class="user-img">
                                            </div>
                                            <div class="d-inline-block">
                                                <h3 class="username"> {{$message->user->name}} </h3>
                                                <span class="time">  </span>
                                            </div>
                                        </div>
                                        <div class="msg" id="lastChatting">
                                            <p> {{$message->message}}  </p>

                                            @if($message->pathInfo !== '')
                                                @if($message->pathInfo == 'jpg' || $message->pathInfo == 'png'||$message->pathInfo == 'jpeg')
                                                    <a data-fancybox="images"
                                                       href="{{ $helper->getDefaultImage(request()->root().'/'. $message->url, request()->root().'/public/assets/admin/images/default.png') }}">
                                                        <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                             src="{{ $helper->getDefaultImage(request()->root().'/'. $message->url, request()->root().'/public/assets/admin/images/default.png') }}"/>
                                                    </a>
                                                @elseif($message->pathInfo == 'docx')
                                                    <a data-type="iframe"  onclick= target="_blank"
                                                       href="{{ $helper->getDefaultImage(request()->root().'/' .$message->url, request()->root().'/public/assets/admin/images/default.png') }}">
                                                        <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                             src="{{ $helper->getDefaultImage(request()->root().'/public/doxc.png', request()->root().'/public/assets/admin/images/default.png') }}"/>
                                                    </a>
                                                @else
                                                    <a data-type="iframe"  onclick= target="_blank"
                                                       href="{{ $helper->getDefaultImage(request()->root().'/' .$message->url, request()->root().'/public/assets/admin/images/default.png') }}">
                                                        <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                             src="{{ $helper->getDefaultImage(request()->root().'/public/pdf.jpg', request()->root().'/public/assets/admin/images/default.png') }}"/>
                                                    </a>
                                                @endif

                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <img src="{{Url('/') .'/public/emptyMessage.png'}}" class="img-responsive  "  >

                            @endif
                            <div class="msg_field">

                                @if($conversation->status == 'open')
                                    <form id="submitChatting" enctype="multipart/form-data" method="post" action="{{route('product-conversations-orders.store')}}" >
                                        {{ csrf_field() }}

                                        <textarea id="chatSend" name="chat_message" placeholder="اكتب رسالتك" class="chat_field chat_message"></textarea>

                                        <input type="hidden" name="convId" value="{{request('convId') }}">
                                        <a id="msg_send" onclick="$(this).closest('form').submit()">
                                            {{--<a id="msg_send"  >--}}
                                            <i class="fas fa-paper-plane"></i>
                                        </a>


                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <div class="input-group image-preview">
                                            <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                            <span class="input-group-btn">
                                                <!-- image-preview-clear button -->
                                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                    <span class="glyphicon glyphicon-remove"></span> Clear
                                                </button>
                                                <!-- image-preview-input -->
                                                <div class="btn btn-default image-preview-input">
                                                    <span class="glyphicon glyphicon-folder-open"></span>
                                                    <span class="image-preview-input-title">Browse</span>
                                                    <input type="file"   id="file-input" name="fileUpload"/> <!-- rename it -->
                                                </div>
                                            </span>
                                        </div>
                                        <!-- /input-group image-preview [TO HERE]-->
                                    </form>
                                @else

                                    <textarea class="text-center"   placeholder="تم إغلاق هذه المحادثة" readonly ></textarea>


                                @endif

                            </div>
                        </div>

                    </div>


                </div>
                @if($product)

                <div class="col-sm-4">
                    <div class="card-box text-center">
                        <div class="product-img">
                            @if(pathinfo( $product->images[0]['url'], PATHINFO_EXTENSION) == 'pdf')

                                <a data-fancybox="gallery"  href="{{  $helper->getDefaultImage( request()->root().$product->images[0]['url'] , request()->root().'/public/pdf.jpg') }}">
                                    <img style="width: 50%; height: 50%;"
                                         src="{{ $helper->getDefaultImage( request()->root().'/public/pdf.jpg' , request()->root().'/public/pdf.jpg') }}"/>
                                </a>
                            @else

                                <a data-fancybox="gallery"  href="{{  $helper->getDefaultImage( request()->root().$product->images[0]['url'] , request()->root().'/public/pdf.jpg') }}">
                                    <img style="width: 50%; height: 50%;"
                                         src="{{ $helper->getDefaultImage( request()->root().$product->images[0]['url'] , request()->root().'/public/pdf.jpg') }}"/>
                                </a>

                            @endif
                        </div>
                        <div class="product-details">
                            <h2 class="product-name">
                                {{$product->name}}
                            </h2>
                            <h3 class="provider-name">
                                {{$product->user->name}}
                            </h3>
                            <span class="product-price">
 {{$product->price}} ريال
                            </span>
                            <p class="desc"> {{$product->description}}  </p>
                        </div>

                    </div>


                </div>
                @endif
            </div>
            <!-- end row -->



        </div>
        <!-- end container -->

    </div>


@endsection

@section('scripts')

    <script>

        $(".msg-block").animate({ scrollTop: $('.msg-block').prop("scrollHeight")}, 1000);

        var id      = "{{ Auth::user()->id }}",
            name    = "{{ Auth::user()->name }}",
            convId  = "{{ request('convId') }}",
            image   = "{{request()->root(). Auth::user()->image }}";
        //            file =    $("#file-input").val();


        //User msg
        function userSend(text) {

            if (text !== ''){
                $('#chat_converse').append('<div class="msg-block"><div class="d-flex"><div class="d-inline-block"><img src="'+image+'" class="user-img"></div><div class="d-inline-block"><h3 class="username">'+name+'</h3><span class="time"> </span></div></div> <div class="msg"><p>' + text + '</p></div></div></div>');
            }

            if ($(' .chat_converse').height() >= 256) {
                $('.chat_converse').addClass('is-max');
            }

            $('.chat_converse').scrollTop($('.chat_converse')[0].scrollHeight);

        }
        //Send input using enter and send key
        $('#chatSend').bind("enterChat", function(e) {
            userSend($('#chatSend').val());
        });
        $('#msg_send').bind("enterChat", function(e) {
            userSend($('#chatSend').val());
        });

        $('#chatSend').keypress(function(event) {
            file =    $("#file-input").val();
            if (event.keyCode === 13) {
                event.preventDefault();
                if (jQuery.trim($(' #chatSend').val()) !== '' || file !== '') {
                    $(this).trigger("enterChat");
                    $('#submitChatting').submit();

                }
            }
        });

        $('#msg_send').click(function(e) {
            file =    $("#file-input").val();
            if (jQuery.trim($(' #chatSend').val()) !== '' || file !== '') {
                $(this).trigger("enterChat");
            }
        });


        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);
            console.log( $("#file-input").val())

            if ($('#chatSend').val() !== '' || $("#file-input").val() !== '' ){


                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    beforeSend:function () {
                        $('.loading').show();
                    },
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                        $('.image-preview').attr("data-content","").popover('hide');
                        $('.image-preview-filename').val("");
                        $('.image-preview-clear').hide();
                        $('.image-preview-input input:file').val("");
                        $(".image-preview-input-title").text("Browse");
                        $('#chatSend').val('');

                    },
                    error: function (data) {
                    }
                });
            }else {
                $('.loading').hide();

                var shortCutFunction = 'error';
                var msg = 'يجب إرسال نص او إرفاق صورة / مستند';
                var title = 'خطأ';
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;
            }
        });

        {{--function addMessages (messageText, file) {--}}


            {{--var url = '{{route('product-conversations-orders.store')}}';--}}
            {{--var formData = new FormData();--}}
            {{--console.log(file)--}}
            {{--$.ajax({--}}
                {{--type:'post',--}}
                {{--url :url,--}}
                {{--data: formData,--}}
                {{--cache: false,--}}
                {{--contentType: false,--}}
                {{--processData: false,--}}
                {{--success:function(data){--}}

                    {{--return data;--}}

                {{--}--}}
            {{--});--}}
        {{--};--}}



        $(document).on('click', '#close-preview', function(){
            $('.image-preview').popover('hide');
            // Hover befor close the preview
            $('.image-preview').hover(
                function () {
                    $('.image-preview').popover('show');
                },
                function () {
                    $('.image-preview').popover('hide');
                }
            );
        });

        $(function() {
            // Create the close button
            var closebtn = $('<button/>', {
                type:"button",
                text: 'x',
                id: 'close-preview',
                style: 'font-size: initial;',
            });
            closebtn.attr("class","close pull-right");
            // Set the popover default content
            $('.image-preview').popover({
                trigger:'manual',
                html:true,
                title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
                content: "There's no image",
                placement:'bottom'
            });
            // Clear event
            $('.image-preview-clear').click(function(){
                $('.image-preview').attr("data-content","").popover('hide');
                $('.image-preview-filename').val("");
                $('.image-preview-clear').hide();
                $('.image-preview-input input:file').val("");
                $(".image-preview-input-title").text("Browse");
            });
            // Create the preview image
            $(".image-preview-input input:file").change(function (){
                var img = $('<img/>', {
                    id: 'dynamic',
                    width:250,
                    height:200
                });
                var file = this.files[0];
                var reader = new FileReader();
                // Set preview image into the popover data-content
                reader.onload = function (e) {
                    $(".image-preview-input-title").text("Change");
                    $(".image-preview-clear").show();
                    $(".image-preview-filename").val(file.name);
                    img.attr('src', e.target.result);
                    $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
                reader.readAsDataURL(file);
            });
        });


    </script>

@endsection