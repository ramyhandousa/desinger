@extends('Provider.layouts.master')

@section('content')


    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> ﺭﺟﻮﻉ <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>

                </div>
                <h4 class="page-title">ﺑﻴﺎﻧﺎﺕ اﻟﻄﻠﺐ</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                <div class="row">
                    <h4 class="header-title m-t-0 m-b-30"> بيانات الطلب </h4>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName">ﺭﻗﻢ  اﻟﻄﻠﺐ*</label>
                            <input type="text" name="name" value="{{ $order->id  }}" class="form-control"
                                   readonly
                                   placeholder="اﺳﻢ اﻟﻤﺴﺘﺨﺪﻡ ﺑﺎﻟﻜﺎﻣﻞ..."/>

                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName">ﺇﺳﻢ اﻟﻤﺴﺘﺨﺪﻡ*</label>
                            <input type="text" name="name" value="{{  optional($order->user)->name   }}" class="form-control"
                                   readonly
                                   placeholder="اﺳﻢ اﻟﻤﺴﺘﺨﺪﻡ ﺑﺎﻟﻜﺎﻣﻞ..."/>

                        </div>
                    </div>


                    {{--<div class="col-xs-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="userPhone">إسم مزود الخدمة *</label>--}}
                            {{--<input type="text" name="phone" value="{{  optional($order->provider)->name  }}"--}}
                                   {{--class="form-control" readonly--}}
                                   {{--placeholder="ﺭﻗﻢ اﻟﺠﻮاﻝ..."/>--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">إسم الخدمة*</label>
                            <input type="text" name="phone" value="{{ optional($order->product)->name  }}"
                                   class="form-control" readonly
                                   placeholder="ﺭﻗﻢ اﻟﺠﻮاﻝ..."/>
                        </div>
                    </div>




                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">ﺣﺎﻟﺔ  اﻟﻄﻠﺐ  *</label>
                            <input type="text" name="phone" value="@switch($order->status)
                                @case('pending') ﺟﺪﻳﺪ  @break
                                @case('preparing') ﺟﺎﺭﻱ اﻟﺘﺠﻬﻴﺰ @break
                                @case('accepted') ﺗﻢ اﻟﻤﻮاﻓﻘﺔ @break
                                @case('finish') منتهي @break
                                @case('refuse') مرفوض @break
                                @endswitch"
                                   class="form-control" readonly
                                   placeholder="ﺭﻗﻢ اﻟﺠﻮاﻝ..."/>
                        </div>
                    </div>


                  @if($order->status == 'refuse')
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">سبب الرفض *</label>
                            <input type="text" name="phone" value="{{$order->refuse_reason == null ? "" :$order->refuse_reason}}"
                                   class="form-control" readonly
                                   placeholder="ﺭﻗﻢ اﻟﺠﻮاﻝ..."/>
                        </div>
                    </div>
                @endif

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">ﺳﻌﺮ  اﻟﻄﻠﺐ  *</label>
                            <input type="text" name="phone" value="{{ $order->price  }}"
                                   class="form-control" readonly
                                   placeholder="ﺭﻗﻢ اﻟﺠﻮاﻝ..."/>
                        </div>
                    </div>
                </div>



                </div>

            </div>


        </div>
        <!-- end row -->


@endsection

