@extends('Provider.layouts.master')

@section('title',"ﻃﻠﺒﺎﺕ اﻟﻤﻨﺘﺠﺎﺕ")

@section('content')


    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
          {{--   <div class="btn-group pull-right m-t-15 ">
                <a href="{{ route('products.create') }}"
                   type="button" class="btn btn-custom waves-effect waves-light"
                   aria-expanded="false">
                <span class="m-l-5">
                <i class="fa fa-plus"></i>
                </span>
                    ﺇﺿﺎﻓﺔ ﻣﻨﺘﺞ
                </a>
            </div> --}}
            <h4 class="page-title">   طلبات المنتجات</h4>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <div class="dropdown pull-right">
                    {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--<i class="zmdi zmdi-more-vert"></i> --}}
                    {{--</a>--}}

                </div>

                <h4 class="header-title m-t-0 m-b-30">ﻛﻞ الطلبات</h4>

                <table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>اﻟﻤﻨﺘﺞ</th>
                        <th>ﺇﺳﻢ اﻟﻤﺴﺘﺨﺪﻡ</th>
                        <th>اﻟﺴﻌﺮ</th>
                        <th>ﺣﺎﻟﺔ اﻟﻄﻠﺐ</th>
                        <th>ﺗﺎﺭﻳﺦ اﻟﻄﻠﺐ</th>
                        <th>ﺧﻴﺎﺭاﺕ</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($orders as $row)
                        <tr>
                                <td>{{ optional($row->product)->name }}</td>
                                <td>{{ optional($row->user)->name }}</td>
                                <td>{{ $row->price }}</td>

                                <td>

                                    @switch($row->status)
                                    @case('pending') ﺟﺪﻳﺪ  @break
                                    @case('preparing') ﺟﺎﺭﻱ اﻟﺘﺠﻬﻴﺰ @break
                                    @case('accepted') ﺗﻢ اﻟﻤﻮاﻓﻘﺔ @break
                                    @case('finish') منتهي @break
                                    @case('refuse') مرفوض@break
                                    @endswitch

                                </td>


                                <td>
                                    {{ $row->created_at->format('Y-m-d') }}
                                </td>

                  

                            <td>

                                <a href="{{ route('orders-products.show', $row->id) }}"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="ﺗﻔﺎﺻﻴﻞ"
                                   class="btn btn-icon btn-xs waves-effect  btn-warning">
                                    <i class="fa fa-eye"></i>
                                </a>

                                @if($row->status =='pending')
                                  <a href="javascript:;" data-toggle="modal"  data-html="{{$row->created_at}}" data-target="#con-close-modal" data-id="{{$row->id}}" data-action="suspend"
                                   data-url="{{route('orders-products.refuse')}}" 
                                   class="suspendWithReason label label-danger">رفض</a>

    
                                    <a id="elementRow{{$row->id}}" href="javascript:;" data-html="{{$row->created_at}}" data-id="{{$row->id}}" data-action="activate"
                                    data-url="{{route('orders-products.accept')}}"
                                    class="suspendOrActivate label label-success">موافقة</a>
                                @endif
                                @if($row->status =='accepted' && $row->convId != null)

                                  <a href="{{route('product-conversations-orders.index')}}?convId={{$row->convId->id}}" class="  label label-success" target="_blank">الشات </a>

                                @endif


                              {{--   <a href="javascript:;" data-url="{{ route('products.destroy',$row->id) }}" id="elementRow{{ $row->id }}" data-id="{{ $row->id }}"
                                   class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </a> --}}


                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
      <form id="refuseForm" data-parsley-validate novalidate method="POST" action="{{route('orders-products.refuse')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                    <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">ﺭﻓﺾ اﻟﻄﻠﺐ</h4>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" id="idHolder" name="id">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group no-margin">
                                                <label for="field-7" class="control-label">سبب الرفض</label>
                                                <textarea name="refuse_reason" data-parsley-required data-parsley-required-message="سبب الرفض مطلوب "
                                                          maxlength="250"  class="form-control autogrow" id="refuseField" placeholder="سبب الرفض" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">ﺇﻟﻐﺎء</button>
                                    <button type="submit" class="btn btn-danger waves-effect waves-light">رفض </button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal -->
                </form>
            </div>
        </div>
    </div>



@endsection


@section('scripts')

    <script>

        function vailddate(createdAt) {
            var today = new Date();
            var month = today.getMonth()+1;
            var day = today.getDate();

            var date = today.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' +  (day<10 ? '0' : '') + day;
            if (createdAt <= date){
                var shortCutFunction = 'error';
                var msg = 'للاسف تاريخ اليوم اكبر من تاريخ الطلب  ';
                var title = 'نجاح';
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title);
                $toastlast = $toast;
                return false;
            }
            return true;
        }
         $('body').on('click', '.suspendOrActivate', function () {
            var createdAt = $(this).attr('data-html');
             var  validSuccess = vailddate(createdAt);
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            var action = $(this).attr('data-action');
            var text = '';
            var type = '';
            var confirmButtonClass = '';
            var redirectionRoute = '';
                text = "هل تريد قبول الطلب فعلا ؟";
                type = 'success';
                confirmButtonClass = 'btn-success waves-effect waves-light';
                redirectionRoute = '{{route('orders-products.index')}}';
            
            if (validSuccess){
                swal({
                        title: "هل انت متأكد؟",
                        text: text,
                        type: type,
                        showCancelButton: true,
                        confirmButtonColor: "#27dd24",
                        confirmButtonText: "موافق",
                        cancelButtonText: "إلغاء",
                        confirmButtonClass:confirmButtonClass,
                        closeOnConfirm: true,
                        closeOnCancel: true,
                    },
                    function (isConfirm) {
                        if(isConfirm){
                            $.ajax({
                                type:'post',
                                url :url,
                                data:{id:id},
                                dataType:'json',
                                success:function(data){
                                    if(data.status == true){
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass : 'toast-top-left',
                                            onclick:null
                                        };

                                        var $toast = toastr['success'](msg,title);
                                        $toastlast = $toast;

                                        function pageRedirect() {
                                            window.location.href =redirectionRoute;
                                        }
                                        setTimeout(pageRedirect(), 750);
                                    }else {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass : 'toast-top-left',
                                            onclick:null
                                        };

                                        var $toast = toastr['error'](msg,title);
                                        $toastlast = $toast
                                    }
                                }
                            });
                        }

                    }
                );
            }

        });



   $('body').on('click', '.suspendWithReason', function () {
           var createdAt = $(this).attr('data-html');
           var  validSuccess = vailddate(createdAt);

           if (validSuccess == false){
               $('#con-close-modal').modal({
                   show: 'false'
               });
           }
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
           var idHolder = $('#idHolder').attr('value',id);
             if (validSuccess) {
                 $('#suspendForm').on('submit', function (e) {
                     e.preventDefault();
                     var suspendReason = $('#suspendField').val();

                     $.ajax({
                         type: 'post',
                         url: url,
                         data: {id: id, suspendReason: suspendReason},
                         dataType: 'json',
                         success: function (data) {
                             if (data.status == true) {
                                 var title = data.title;
                                 var msg = data.message;
                                 toastr.options = {
                                     positionClass: 'toast-top-left',
                                     onclick: null
                                 };

                                 var $toast = toastr['success'](msg, title);
                                 $toastlast = $toast

                                 function pageRedirect() {
                                     window.location.href = '{{route('orders-products.index')}}';
                                 }

                                 setTimeout(pageRedirect(), 750);
                             } else {
                                 var title = data.title;
                                 var msg = data.message;
                                 toastr.options = {
                                     positionClass: 'toast-top-left',
                                     onclick: null
                                 };

                                 var $toast = toastr['error'](msg, title);
                                 $toastlast = $toast
                             }
                         }
                     });

                 })
             }
        });


        $(document).ready(function () {
            //$('#datatable').dataTable();
            //$('#datatable-keytable').DataTable( { keys: true } );
            $('#datatable-responsive').DataTable();

        });


    </script>


@endsection



