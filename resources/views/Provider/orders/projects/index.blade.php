@extends('Provider.layouts.master')

@section('title',"طلبات المشاريع")

@section('content')


    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <h4 class="page-title">طلبات المشاريع </h4>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <div class="dropdown pull-right">
                    {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--<i class="zmdi zmdi-more-vert"></i> --}}
                    {{--</a>--}}

                </div>

                <h4 class="header-title m-t-0 m-b-30">ﻛﻞ اﻟﻄﻠﺒﺎﺕ</h4>

                <table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>ﺇﺳﻢ اﻟﻤﺴﺘﺨﺪﻡ</th>
                        <th>حالة الطلب   </th>
                        <th>ﺗﺎﺭﻳﺦ اﻟﻄﻠﺐ</th>
                        <th>ﺧﻴﺎﺭاﺕ</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($orders as $row)
                        <tr>
                            <td>{{ optional($row->user)->name }}</td>
                            <td>  {{  $row->status_translation }}  </td>

                            <td>    {{ $row->created_at->format('Y-m-d') }}  </td>

                            <td>

                                <a href="{{ route('orders-projects.show', $row->id) }}"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="ﺗﻔﺎﺻﻴﻞ"
                                   class="btn btn-icon btn-xs waves-effect  btn-warning">
                                    <i class="fa fa-eye"></i>
                                </a>

                                @if($row->status =='pending')

                                    @if($row->hasOfferPriceByThisProvider() && $row->time_out == 0 && $row->created_at->isToday()  )
                                      <a href="javascript:;" data-toggle="modal"  data-html="{{$row->received_date}}" data-target="#con-close-modal{{$row->id}}" data-id="{{$row->id}}" data-action="suspend"
                                       data-url="{{route('orders-projects.addoffer')}}"
                                       class="suspendWithReason label label-primary">تقديم عرض</a>

                                        <form id="refuseForm" data-parsley-validate novalidate method="POST" action="{{route('orders-projects.addoffer')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div id="con-close-modal{{$row->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h4 class="modal-title">تقديم عرض سعر</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <input type="hidden" id="idHolder" value="{{$row->id}}" name="id">


                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-12" style="margin: 10px">
                                                                        <label for="field-7" class="control-label col-md-4">المدة الكلية  </label>
                                                                        <input class="form-control col-md-6" type="number" oninput="validity.valid||(value='');" name="total_time" min="1" required  data-parsley-required-message="المدة الكلية مطلوبة" >
                                                                        <label for="field-7" class="control-label col-md-2">يوم  </label>
                                                                    </div>
                                                                    <div class="col-md-12" style="margin: 10px">
                                                                        <label for="field-7" class="control-label col-md-4">عدد الدفعات  </label>
                                                                        <input class="form-control col-md-6" type="number" oninput="validity.valid||(value='');" name="batches" min="1" required  data-parsley-required-message="عدد الدفعات  مطلوبة" >
                                                                        <label for="field-7" class="control-label col-md-2">دفعة  </label>

                                                                    </div>
                                                                    <div class="col-md-12" style="margin: 10px">
                                                                        <label for="field-7" class="control-label col-md-4">المدة بين التسليمات  </label>
                                                                        <input class="form-control col-md-6" type="number" oninput="validity.valid||(value='');" name="duration" min="1" required  data-parsley-required-message="المدة بين التسليمات  مطلوبة" >
                                                                        <label for="field-7" class="control-label col-md-2">يوم  </label>

                                                                    </div>
                                                                    <div class="col-md-12" style="margin: 10px">
                                                                        <label for="field-7" class="control-label col-md-4">السعر  </label>
                                                                        <input class="form-control col-md-6" type="number" oninput="validity.valid||(value='');" name="price" min="1" required  data-parsley-required-message="السعر مطلوب" >
                                                                        <label for="field-7" class="control-label col-md-2">ريال  </label>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">إلغاء</button>
                                                            <button type="submit" class="btn btn-success waves-effect waves-light">إرسال العرض</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal -->
                                        </form>

                                    @endif
                                @endif

                                @if( ($row->status =='accepted' ||$row->status =='finish'   )&& $row->conversation != null)

                                    <a href="{{route('projectChatting')}}?convId={{$row->conversation->id}}" class="  label label-success" target="_blank">الشات </a>

                                @endif

                              {{--   <a href="javascript:;" data-url="{{ route('products.destroy',$row->id) }}" id="elementRow{{ $row->id }}" data-id="{{ $row->id }}"
                                   class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </a> --}}


                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->      </div>
        </div>
    </div>



@endsection


@section('scripts')

    <script>


        function vailddate(createdAt) {

            var today = new Date();

            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

            if (date <= createdAt){
                var shortCutFunction = 'error';
                var msg = 'للاسف تاريخ اليوم اكبر من تاريخ الطلب  ';
                var title = 'نجاح';
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title);
                $toastlast = $toast;
                return false;
            }
            return true;
        }


    $('body').on('click', '.suspendOrActivate', function () {

        var createdAt = $(this).attr('data-html');
        var  validSuccess = vailddate(createdAt);


            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            var action = $(this).attr('data-action');
            var text = '';
            var type = '';
            var confirmButtonClass = '';
            var redirectionRoute = '';

                text = "هل تريد قبول الطلب فعلا ؟";
                type = 'success';
                confirmButtonClass = 'btn-success waves-effect waves-light';
                redirectionRoute = '{{route('orders-products.index')}}';


            swal({
                    title: "هل انت متأكد؟",
                    text: text,
                    type: type,
                    showCancelButton: true,
                    confirmButtonColor: "#27dd24",
                    confirmButtonText: "موافق",
                    cancelButtonText: "إلغاء",
                    confirmButtonClass:confirmButtonClass,
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function (isConfirm) {
                    if(isConfirm){
                        $.ajax({
                            type:'post',
                            url :url,
                            data:{id:id},
                            dataType:'json',
                            success:function(data){
                                if(data.status == true){
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['success'](msg,title);
                                    $toastlast = $toast;

//                                    $tr.find('td').fadeOut(100,function () {
//                                        $tr.remove();
//                                    });

//                                    function pageRedirect() {
//                                        window.location.href =redirectionRoute;
//                                    }
//                                    setTimeout(pageRedirect(), 750);
                                }else {
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['error'](msg,title);
                                    $toastlast = $toast
                                }
                            }
                        });
                    }

                }
            );
        });

    </script>


@endsection



