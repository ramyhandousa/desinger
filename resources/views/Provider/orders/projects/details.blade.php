@extends('Provider.layouts.master')

@section('content')


    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <a  href="{{route('orders-projects.index')}}" type="button" class="btn btn-custom  waves-effect waves-light"
                             > ﺭﺟﻮﻉ <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </a>

                </div>
                <h4 class="page-title">ﺑﻴﺎﻧﺎﺕ اﻟﻄﻠﺐ</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                <div class="row">
                    <h4 class="header-title m-t-0 m-b-30"> بيانات الطلب </h4>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName">رقم  اﻟﻄﻠﺐ*</label>
                            <input type="text" name="name" value="{{ $order->id  }}" class="form-control"
                                   readonly
                                   />

                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName">إسم اﻟﻤﺴﺘﺨﺪﻡ*</label>
                            <input type="text" name="name" value="{{  optional($order->user)->name   }}" class="form-control"
                                   readonly
                            />
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="userName">وصف المشروع *</label>
                            <textarea type="text" name="name" class="form-control"
                                   readonly
                                   >{{$order->description}}</textarea>
                        </div>
                    </div>



                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName"> تاريخ الطلب  *</label>
                            <input type="text" name="name" value="  {{ $order->created_at->format('Y-m-d')  }}  " class="form-control"
                                   readonly
                                   />
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">ﺣﺎﻟﺔ  اﻟﻄﻠﺐ  *</label>
                            <input type="text" name="phone" value="{{$order->status_translation}}"
                                   class="form-control" readonly
                                   placeholder="ﺭﻗﻢ اﻟﺠﻮاﻝ..."/>
                        </div>
                    </div>

                  @if($order->status == 'refuse')
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">سبب الرفض *</label>
                            <input type="text" name="phone" value="{{$order->refuse_reason == null ? "" :$order->refuse_reason}}"
                                   class="form-control" readonly
                                   placeholder="ﺭﻗﻢ اﻟﺠﻮاﻝ..."/>
                        </div>
                    </div>
                @endif


                    @if($offer && $offer->price != null)

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="userPhone">العرض المقدم منك على الطلب*</label>
                                <input type="text" name="phone" value="{{ $order->offerByThisProvider()->price  }}"
                                       class="form-control" readonly
                                       placeholder="العرض المقدم منك على الطلب  ..."/>
                            </div>
                        </div>
                    @endif


                    @if($order->images)

                        @foreach($order->images as $image)
                            <div class="col-xs-2 ">
                                <div class="form-group">
                                    <label for="details">    الملفات  </label>
                                    @if(pathinfo($image, PATHINFO_EXTENSION) == 'pdf')
                                        <a data-fancybox="gallery"  href="{{  $helper->getDefaultImage( request()->root().'/'.$image , request()->root().'/pdf.jpg') }}">
                                            <img style="width: 50%; height: 50%;"
                                                 src="{{ $helper->getDefaultImage( request()->root().'/pdf.jpg' , request()->root().'/pdf.jpg') }}"/>
                                        </a>
                                    @else

                                    <a data-fancybox="gallery"  href="{{  $helper->getDefaultImage( request()->root().'/'.$image , request()->root().'/pdf.jpg') }}">
                                        <img style="width: 50%; height: 50%;"
                                             src="{{ $helper->getDefaultImage( request()->root().'/'.$image , request()->root().'/pdf.jpg') }}"/>
                                    </a>

                                    @endif
                                </div>
                            </div>
                        @endforeach

                    @endif

                    </div>



                </div>

            </div>


        </div>
        <!-- end row -->


@endsection

