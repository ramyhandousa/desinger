
@extends('Provider.layouts.master')
@section('title', 'عن التطبيق')
@section('content')



    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">

                    <a href="#" onclick="window.history.back();return false;"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>
                </div>
                <h4 class="page-title">عن التطبيق</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card-box">

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="userName">عن التطبيق</label>
                            <textarea class="form-control" rows="14" readonly>{{\App\Models\Setting::getBody('about_us_ar')}}</textarea>
                            <p class="help-block" id="error_userName"></p>
                            @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                            @endif
                        </div>
                    </div>


                    <div class="form-group text-right m-t-20">
                       {{--  <button class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            حفظ البيانات
                        </button> --}}
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            رجوع
                        </button>
                    </div>

                </div>
            </div><!-- end col -->

            <div class="col-lg-4">
             {{--    <div class="card-box" style="overflow: hidden;">


                    <h4 class="header-title m-t-0 m-b-30">الصورة الشخصية</h4>

                    <div class="form-group">
                     
                    </div>

                </div> --}}
            </div><!-- end col -->
        </div>
        <!-- end row -->
   

@endsection

