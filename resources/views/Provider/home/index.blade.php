@extends('Provider.layouts.master')
@section('title', 'الصفحة الرئيسية')


@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">الرئيسية</h4>
        </div>
    </div>




        <div class="row statistics">



        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">طلبات المشاريع الجديدة</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$order_projects_pending}}</h2>
                            <p class="text-muted m-b-0">طلب</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- end col -->




        <div class="col-lg-3 col-md-6">
            <a href="javascript:;">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">طلبات المشاريع المكتملة</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                            <h2 class="m-b-0">{{$order_projects_finish}}</h2>
                            <p class="text-muted m-b-0">طلب</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- end col -->
    </div>



@endsection
