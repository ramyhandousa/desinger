@extends('Provider.layouts.master')
@section('title',"اﻟﻤﻨﺘﺠﺎﺕ")


@section('styles')

@endsection
@section('content')


    <form method="POST" action="{{ route('previous-works.store') }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> ﺭﺟﻮﻉ <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة سابقة الأعمال</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30">إضافة عمل</h4>

                    <div class="row">


                        <div class="col-xs-8">
                            <div class="form-group">
                                <label for="userName">وصف     *</label>
                                <textarea type="text" name="description_ar"  class="form-control text" required></textarea>
                            </div>
                        </div>

                        <div class="col-xs-4">

                            <label for="usernames">صورة العمل الرئيسية ( الواجهة) </label>
                            <input  type="file" required data-parsley-required-message="صورة العمل الرئيسية " name="master_image"  class="dropify" data-max-file-size="6M"/>

                        </div>



                        <div class="col-xs-12">
                           <div class="col-xs-3">
                               <div class="form-group">
                                   <label for="usernames">صورة العمل   </label>
                                   <input  type="file" required data-parsley-required-message="ﻭاﺣﺪﺓ ﻋﻠﻰ اﻷﻗﻞ من معرضك الخاص " name="image[]"  class="dropify" data-max-file-size="6M"/>
                               </div>
                        </div>

                            @for ($i = 0; $i < 3; $i++)
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <label for="usernames">صورة العمل  </label>
                                        <input type="file" name="image[]" class="dropify" data-max-file-size="6M"/>
                                    </div>
                                </div>
                            @endfor

                        </div>

                    </div>

                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')


    <script type="text/javascript" src="/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">

//        $(document).ready(function() {
//            $(function() {
//                $("#select_date").on('change', function(){
//                    var date = Date.parse($(this).val());
//                    if (date < Date.now()){
//                        var shortCutFunction = 'error';
//                        var msg = 'من فضلك إختار تاريخ قدام ';
//                        var title = 'نجاح';
//                        toastr.options = {
//                            positionClass: 'toast-top-left',
//                            onclick: null
//                        };
//                        var $toast = toastr[shortCutFunction](msg, title);
//                        $toastlast = $toast;
//                        $(this).val('');
//                    }
//                });
//            });
//        });

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();

        });



//        $( '.styled' ).one( "click", function() {
//            var shortCutFunction = 'success';
//            var msg = 'ﺳﻮﻑ ﻳﺘﻢ ﻣﻌﺎﻣﻠﺔ اﻟﻌﺮﺽ ﻋﻠﻲ اﻧﻪ ﻣﻦ ﺿﻤﻦ اﻟﻌﺮﻭﺽ اﻟﻴﻮﻣﻴﺔ ';
//            var title = 'ﻧﺠﺎﺡ';
//            toastr.options = {
//                positionClass: 'toast-top-left',
//                onclick: null
//            };
//            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
//            $toastlast = $toast;
//        });




        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid()){


                $.ajax({
                    beforeSend: function()
                    {
                        $('.loading').show();
                    },
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'ﻧﺠﺎﺡ';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 2000);
                    },
                    error: function (data) {
                        $('.loading').hide();

                        console.log(data)
                        var shortCutFunction = 'error';
                        var msg = data.responseJSON.error[0];
                        var title = 'فشل';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                    }
                });
            }else {
                $('.loading').hide();
            }
        });

    </script>
@endsection

