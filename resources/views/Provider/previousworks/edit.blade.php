@extends('Provider.layouts.master')
@section('title',"ﺞﺘﻨﻣ ﻞﻳﺪﻌﺗ")


@section('styles')


@endsection
@section('content')


    <form method="POST" action="{{ route('previous-works.update',$product->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate  >
            {{ csrf_field() }}
        {{ method_field('PUT') }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    <a href="{{route('previous-works.index')}}" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> ﺭﺟﻮﻉ <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </a>
                </div>
                <h4 class="page-title">تعديل سابقة الأعمال</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30">تعديل  سابقة العمل </h4>

                    <div class="row">



                        <div class="col-xs-8">
                            <div class="form-group">
                                <label for="userName">وصف     *</label>
                                <textarea type="text" name="description_ar"  class="form-control text" required>
                                    {{ $product->description }}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-xs-4">

                            <label for="usernames">صورة العمل الرئيسية ( الواجهة) </label>
                            <input  type="file" data-show-remove="false" data-allowed-file-extensions="JPEG  png JPG"   data-default-file="{{ URL('/').'/'. $product->image  }}"
                                    name="master_image" id="master_image"  class="dropify" data-max-file-size="6M"/>

                        </div>



                        <div class="col-xs-12">

                            @if(count($product->images) > 0)
                                @foreach($product->images as $image)

                                    <div class="col-xs-3">
                                        <div class="form-group" id="image-works{{$image->id}}">
                                            <a href="javascript:;" data-url="{{ route('previous-works.deleteImage') }}" id="elementRow{{ $image->id }}" data-id="{{ $image->id }}"
                                               class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                            <label for="usernames">صورة سابقة الأعمال  </label>

                                            @if(pathinfo($image['url'], PATHINFO_EXTENSION) == 'pdf')
                                                <a data-fancybox="gallery"  href="{{  $helper->getDefaultImage( request()->root().'/'. $image['image'] , request()->root().'/public/pdf.jpg') }}">
                                                    <img style="width: 50%; height: 30%;"
                                                         src="{{ $helper->getDefaultImage( request()->root().'/public/pdf.jpg' , request()->root().'/public/pdf.jpg') }}"/>
                                                </a>
                                            @else
                                                <a data-fancybox="gallery"  href="{{  $helper->getDefaultImage( request()->root().'/'. $image['image'] , request()->root().'/public/pdf.jpg') }}">
                                                    <img style="width: 50%; height: 50%;"
                                                         src="{{ $helper->getDefaultImage( request()->root().'/'. $image['image'] , request()->root().'/public/pdf.jpg') }}"/>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <input type="hidden" name="oldImages[]" value="{{$image['image'] }}">
                                @endforeach
                            @endif
                            @for ($i = 0; $i < 4 - count($product->images); $i++)
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <label for="usernames">صورة  جديدة      </label>
                                        <input type="file" name="image[]" data-allowed-file-extensions="JPEG  png JPG" class="dropify" data-max-file-size="6M"/>
                                    </div>
                                </div>
                            @endfor

                        </div>

                    </div>

                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')



    <script type="text/javascript" src="/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">


            function validImages() {

                var oldImages = $( "input[name='oldImages[]']" ).val();
                var images = $( "input[name='image[]']" ).val();


                if ( (images === undefined || images == "" ) &&  (oldImages === undefined || oldImages == "" )  ) {
                    var shortCutFunction = 'error';
                    var msg = 'من فضلك إختار   صورة واحدة علي الأقل  ';
                    var title = 'نجاح';
                    toastr.options = {
                        positionClass: 'toast-top-left',
                        preventDuplicates: false,
                        onclick: null
                    };
                    var $toast = toastr[shortCutFunction](msg, title);
                    $toastlast = $toast;
                    return false;
                }

                return true;
            }



            function masterImage() {

                var images = $( "input[name='image[]']" ).val();


                if ( (images === undefined || images == "" ) &&  (oldImages === undefined || oldImages == "" )  ) {
                    var shortCutFunction = 'error';
                    var msg = 'من فضلك إختار   صورة واحدة من المعرض الخاص بك  ';
                    var title = 'نجاح';
                    toastr.options = {
                        positionClass: 'toast-top-left',
                        preventDuplicates: false,
                        onclick: null
                    };
                    var $toast = toastr[shortCutFunction](msg, title);
                    $toastlast = $toast;
                    return false;
                }

                return true;
            }




        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();

        });

        $('body').on('click', '.removeElement', function () {


            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            swal({
                title: "هل انت متأكد؟",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status == true) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

//                                $("#image-works" + id)[0].children[2].children[4].click();
                                window.location.reload();

//                                $("#elementRow" + id).remove();
                            }
                            if (data.status == false) {
                                var shortCutFunction = 'warning';
                                var msg =data.message;
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                            }
                        }
                    });
                }
            });
        });


        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid() && validImages()){

                $.ajax({
                    type: 'POST',
                    beforeSend: function()
                    {
                        $('.loading').show();

                    },
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'ﻧﺠﺎﺡ';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    },
                    error: function (data) {
                        $('.loading').hide();

                        var shortCutFunction = 'error';
                        var msg = data.responseJSON.error[0];
                        var title = 'فشل';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                    }
                });
            }else {
                $('.loading').hide();
            }
        });

    </script>
@endsection

