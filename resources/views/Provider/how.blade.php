
@extends('Provider.layouts.master')
@section('title', 'آليه عمل التطبيق')
@section('content')



    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">

                    <a href="#" onclick="window.history.back();return false;"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>
                </div>
                <h4 class="page-title">آليه عمل التطبيق</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card-box">

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="userName">فيديو آليه عمل  التطبيق</label>
                             <video id="videoPlayer" width="100%" height="50%" controls>
                                <source src="{{\App\Models\Setting::getBody('how-to-video')}}" type="video/mp4">
                            </video>

                            
                        
                        </div>
                    </div>


                    <div class="form-group text-right m-t-20">
                       {{--  <button class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            حفظ البيانات
                        </button> --}}
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            رجوع
                        </button>
                    </div>

                </div>
            </div><!-- end col -->

            <div class="col-lg-4">
             {{--    <div class="card-box" style="overflow: hidden;">


                    <h4 class="header-title m-t-0 m-b-30">الصورة الشخصية</h4>

                    <div class="form-group">
                     
                    </div>

                </div> --}}
            </div><!-- end col -->
        </div>
        <!-- end row -->
   

@endsection

