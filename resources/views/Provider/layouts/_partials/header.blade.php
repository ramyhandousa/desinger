<!-- Navigation Bar-->
<header id="topnav">

    <div class="topbar-main">
        <div class="container">

            <!-- LOGO -->
            <div class="topbar-left">
                {{--<a href="{{ route('admin.home') }}" class="logo" style="width: 150px;">--}}
                    {{--<img style="width: 100%" src="{{ request()->root() }}/public/assets/admin/images/logo.png"></a>--}}
            </div>
            <!-- End Logo container-->


            <div class="menu-extras">

                <ul class="nav navbar-nav navbar-right pull-right">

                    <li>
                        <!-- Notification -->
                        <div class="notification-box">
                            <ul class="list-inline m-b-0">
                                <li>
                                    <a href="javascript:void(0);" class="right-bar-toggle">
                                        <i class="zmdi zmdi-notifications-none"></i>
                                    </a>
                                    <div class="noti-dot">
                                        <span class="dot"></span>
                                        <span class="pulse"></span>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <!-- End Notification bar -->
                    </li>

                    <li class="dropdown user-box">
                        <a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown"
                           aria-expanded="true">
                            @if(!auth()->user()->image)
                                     <img src="{{  request()->root().'/default.png'}}"
                                     alt="user-img" class="img-circle user-img">
                                @else
                                    <img src="{{ request()->root().'/'.auth()->user()->image }}"
                                     alt="user-img" class="img-circle user-img">
                                @endif

                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('gym_profile.show', auth()->user()->id)}}">
                                    <i class="ti-user m-r-5"></i>@lang('maincp.personal_page')
                                </a>
                            </li>

                             <li>
                                 <a href="{{ route('gym_profile.edit', auth()->id()) }}">
                                     <i class="ti-settings m-r-5"></i>
                                    تعديل بياناتي
                               </a>
                             <li>
                             <li>
                                 <a href="#">
                                     <i class="ti-settings m-r-5"></i>
                                     <button type="hidden"  data-toggle="modal"
                                             data-target="#myModal">                                     تغير  كلمة المرور

                                     </button>

                               </a>
                             </li>

                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="ti-power-off m-r-5"></i>@lang('maincp.log_out')
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

        </div>
    </div>

    <form id="logout-form" action="{{ route('Provider.logout') }}" method="POST"
          style="display: none;">
        {{ csrf_field() }}
    </form>


    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu" style="    font-size: 12px;">


                    <li>
                        <a href="{{ route('Provider.home') }}"><i class="zmdi zmdi-view-dashboard"></i>
                            <span> الرئيسية </span> </a>
                    </li>



                    <li class="has-submenu">
                        <a href="{{route('orders-projects.index')}}">
                            <i class="zmdi zmdi-square-o"></i>
                            <span>  الطلبات  </span>
                        </a>

                    </li>

                    <li class="has-submenu">
                        <a href="{{route('previous-works.index')}}" >
                            <i class="zmdi zmdi-square-o"></i>
                            <span>سابقة الأعمال</span>
                        </a>
                    </li>

                    <li class="has-submenu">
                        <a href="{{ route('information_admin') }}">
                            <i class="fa fa-info"></i>
                            <span>  المعلومات الخاصة بالتطبيق       </span>
                        </a>
                    </li>
                    <li class="has-submenu">
                        <a href="{{ route('contact_us_inbox.index') }}">
                            <i class="zmdi zmdi-email"></i>
                            <span>  البريد الإلكتروني       </span>
                        </a>
                    </li>

                </ul>
                <!-- End navigation menu  -->
            </div>
        </div>
    </div>

</header>
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container">
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> تغير كلمة المرور</h4>
                    </div>
                    <form   id="changePass"  method="POST" action="{{route('Provider.changePassword')}}" >
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <label for="current_password" class="control-label"> كلمة المرور الحالية  </label>
                            <div class="controls">
                                <input class="form-control" required data-parsley-required-message="من فضلك اكتب الباسورد القديم لديك " type="password" name="oldPassword"><br>
                            </div>
                            <label for="new_password" class="control-label">  كلمة المرور الجديدة</label>
                            <div class="controls">
                                <input  class="form-control" type="password"
                                        data-parsley-required-message="من فضلك اكتب كلمة المرور الجديدة " required
                                        id="password" name="newPassword"
                                        data-parsley-maxlength="20"
                                        data-parsley-minlength="5"
                                        data-parsley-maxlength-message=" أقصى عدد الحروف المسموح بها هى (20) حرف"
                                        data-parsley-minlength-message=" أقل عدد الحروف المسموح بها هى (5) حرف"
                                ><br>
                            </div>
                            <label for="confirm_password" class="control-label">  تأكيد كلمة المرور الجديدة</label>
                            <div class="controls">
                                <input class="form-control" type="password"
                                       required data-parsley-required-message="من فضلك اكتب تاكيد كلمة المرور "
                                       id="password-verify" name="sameNewPassword"
                                       data-parsley-equalto="#password"
                                       data-parsley-equalto-message =' غير مطابقة لكلمة المرور الجديدة'
                                       data-parsley-maxlength="20"
                                       data-parsley-minlength="5"
                                       data-parsley-maxlength-message=" أقصى عدد الحروف المسموح بها هى (20) حرف"
                                       data-parsley-minlength-message=" أقل عدد الحروف المسموح بها هى (5) حرف"
                                >
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default"  >تغير</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"> إلغاء</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
