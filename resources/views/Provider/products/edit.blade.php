@extends('Provider.layouts.master')
@section('title',"ﺞﺘﻨﻣ ﻞﻳﺪﻌﺗ")


@section('styles')


    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">


@endsection
@section('content')


    <form method="POST" action="{{ route('products.update',$product->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
            {{ csrf_field() }}
        {{ method_field('PUT') }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    <a href="{{route('orders-products.index')}}" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> ﺭﺟﻮﻉ <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </a>
                </div>
                <h4 class="page-title">تعديل منتج</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30">تعديل  ﻣﻨﺘﺞ</h4>

                    <div class="row">


                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName"> اﻟﻘﺴﻢ  *</label>
                                <select name="category_id" id="" class="form-control requiredFieldWithMaxLenght" required>
                                    <option value="" selected disabled="">ﺇﺧﺘﺎﺭ اﻟﻘﺴﻢ</option>
                                    @if($categories->count() > 0)
                                                @foreach($categories as $value)
                                                    <option value="{{ $value->id }}" @if($product->category_id == $value->id) selected @endif >{{ $value->name }}</option>
                                                @endforeach
                                        @else
                                    <option disabled selected  >ﻻ ﻳﻮﺟﺪ ﺃﻗﺴﺎﻡ ﻣﺘﺎﺣﺔ ﺣﺎﻟﻴﺎً ﻟﻚ</option>
                                        @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">ﺇﺳﻢ اﻟﻤﻨﺘﺞ ﺑﺎﻟﻌﺮﺑﻴﺔ *</label>
                                <input type="text" name="name_ar"  class="form-control 
                                requiredFieldWithMaxLenght"
                                value="{{ optional($product->translate('ar'))->name }}" 
                                 required>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">ﺇﺳﻢ اﻟﻤﻨﺘﺞ ﺑﺎﻹﻧﺠﻠﻴﺰﻳﺔ *</label>
                                <input type="text" name="name_en"  class="form-control requiredFieldWithMaxLenght"
                                value="{{ optional($product->translate('en'))->name }}"  required>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">ﻭﺻﻒ اﻟﻤﻨﺘﺞ ﺑﺎﻟﻌﺮﺑﻴﺔ *</label>
                                <textarea type="text" name="description_ar"  class="form-control requiredFieldWithMaxLenght" required>
                                    {{ optional($product->translate('ar'))->description }}
                                </textarea>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">ﻭﺻﻒ اﻟﻤﻨﺘﺞ ﺑﺎﻹﻧﺠﻠﻴﺰﻳﺔ *</label>
                                <textarea type="text" name="description_en"  class="form-control requiredFieldWithMaxLenght" required>
                                    {{ optional($product->translate('en'))->description }}
                                </textarea>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">ﺳﻌﺮ اﻟﻤﻨﺘﺞ *</label>
                                <input type="number" name="price"  min=1 oninput="validity.valid||(value='');" class="form-control
                                requiredFieldWithMaxLenght"
                                value="{{$product->price}}" required>

                            </div>
                        </div>

                        <div class="col-xs-12">
                            @if(count($product->images) > 0)
                                @foreach( $product->images->where('paid',1) as $image)

                                    <div class="col-xs-2">
                                        <div class="form-group" id="image-works{{$image->id}}">
                                            <a href="javascript:;" data-url="{{ route('products.deleteImage') }}" id="elementRow{{ $image->id }}" data-id="{{ $image->id }}"
                                               class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                            <label for="usernames">صورة    المنتج  </label>
                                            <input type="file" id="my_image_remove{{$image->id}}"  data-default-file="{{ request()->root().$image['url'] }}" name="image[{{$image->id}}]"
                                                   class="dropify" data-max-file-size="6M"/>
                                            <input type="hidden" name="oldImage[{{ $image->id }}]" value="{{ request()->root().$image['url'] }}">
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            @for ($i = 0; $i < 5 - count($product->images->where('paid',1)); $i++)
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label for="usernames">صورة  جديدة      </label>
                                        <input type="file" name="image[]" class="dropify" data-max-file-size="1M"/>
                                    </div>
                                </div>
                            @endfor

                        </div>


                        <div class="col-xs-12">
                            @foreach( $product->images->where('paid',0) as $image)

                                <div class="col-xs-2">
                                    <div class="form-group" id="image-works{{$image->id}}">
                                        <a href="javascript:;" data-url="{{ route('products.deleteImage') }}" id="elementRow{{ $image->id }}" data-id="{{ $image->id }}"
                                           class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                        <label for="usernames">     الملفات  </label>
                                        <input type="file" id="my_image_remove{{$image->id}}"  data-default-file="{{ request()->root().$image['url'] }}" name="files[{{$image->id}}]"
                                               class="dropify" data-max-file-size="10M"/>
                                        <input type="hidden" name="oldImage[{{ $image->id }}]" value="{{ request()->root().$image['url'] }}">
                                    </div>
                                </div>
                            @endforeach

                                @for ($i = 0; $i < 2 - count($product->images->where('paid',0)); $i++)
                                    <div class="col-xs-2">
                                        <div class="form-group">
                                            <label for="files"> الملفات   </label>
                                            <input type="file" name="files[]" class="dropify" data-max-file-size="10M"/>
                                        </div>
                                    </div>
                                @endfor
                        </div>
                    </div>

                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    <script src="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>


    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });

        $('body').on('click', '.removeElement', function () {

            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            swal({
                title: "هل انت متأكد؟",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status == true) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;
                                $("#image-works" + id)[0].children[2].children[4].click()
                                $("#elementRow" + id).remove();

                                setTimeout(function () {
                                    window.location.reload();
                                }, 500);
                            }
                            if (data.status == false ) {
                                var shortCutFunction = 'warning';
                                var msg =data.message;
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                            }
                        }
                    });
                }
            });
        });

        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid()){
                $('.loading').show();

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                        if (data.status == false){

                            var shortCutFunction = 'error';
                            var msg = data.message;
                            var title = 'فشل';
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                            $toastlast = $toast;

                        }else {

                            var shortCutFunction = 'success';
                            var msg = data.message;
                            var title = 'ﻧﺠﺎﺡ';
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                            $toastlast = $toast;
                            setTimeout(function () {
                                window.location.href = data.url;
                            }, 2000);
                        }

                    },
                    error: function (data) {
                        $('.loading').hide();
                         var shortCutFunction = 'error';
                        var msg = data.responseJSON.message;
                        var title = 'فشل';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                    }
                });
            }else {
                $('.loading').hide();
            }
        });

    </script>
@endsection

