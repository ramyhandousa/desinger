@extends('Provider.layouts.master')
@section('title',"اﻟﻤﻨﺘﺠﺎﺕ")


@section('styles')


    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">


@endsection
@section('content')


    <form method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> ﺭﺟﻮﻉ <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة المنتجات  </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30">ﺇﺿﺎﻓﺔ ﻣﻨﺘﺞ</h4>

                    <div class="row">


                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName"> اﻟﻘﺴﻢ  *</label>
                                <select name="category_id" id="" class="form-control requiredFieldWithMaxLenght" required>
                                    <option value="" selected disabled="">ﺇﺧﺘﺎﺭ اﻟﻘﺴﻢ</option>
                                    @if($categories->count() > 0)
                                                @foreach($categories as $value)
                                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                @endforeach
                                        @else
                                    <option disabled selected  >ﻻ ﻳﻮﺟﺪ ﺃﻗﺴﺎﻡ ﻣﺘﺎﺣﺔ ﺣﺎﻟﻴﺎً ﻟﻚ</option>
                                        @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">ﺇﺳﻢ اﻟﻤﻨﺘﺞ ﺑﺎﻟﻌﺮﺑﻴﺔ *</label>
                                <input type="text" name="name_ar" value="{{old('name_ar')}}" class="form-control requiredFieldWithMaxLenght" required>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">ﺇﺳﻢ اﻟﻤﻨﺘﺞ ﺑﺎﻹﻧﺠﻠﻴﺰﻳﺔ *</label>
                                <input type="text" name="name_en"  value="{{old('name_en')}}" class="form-control requiredFieldWithMaxLenght" required>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">ﻭﺻﻒ اﻟﻤﻨﺘﺞ ﺑﺎﻟﻌﺮﺑﻴﺔ *</label>
                                <textarea type="text" name="description_ar"    class="form-control requiredFieldWithMaxLenght" required>{{old('description_ar')}}</textarea>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">وصف  اﻟﻤﻨﺘﺞ ﺑﺎﻹﻧﺠﻠﻴﺰﻳﺔ *</label>
                                <textarea type="text" name="description_en"  class="form-control requiredFieldWithMaxLenght" required>{{old('description_en')}}</textarea>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">ﺳﻌﺮ اﻟﻤﻨﺘﺞ *</label>
                                <input type="number" name="price" value="{{old('price')}}"  min=1 oninput="validity.valid||(value='');" class="form-control requiredFieldWithMaxLenght" required>

                            </div>
                        </div>

                        <div class="col-xs-12">
                           <div class="col-xs-2">
                               <div class="form-group">
                                   <label for="usernames">صورة  اﻟﻤﻨﺘﺞ  </label>
                                   <input  type="file" required data-parsley-required-message="ﻭاﺣﺪﺓ ﻋﻠﻰ اﻷﻗﻞ  " name="image[]"  class="dropify" data-max-file-size="6M"/>

                               </div>
                        </div>

                            @for ($i = 0; $i < 4; $i++)
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label for="usernames">صورة  اﻟﻤﻨﺘﺞ  </label>
                                        <input type="file" name="image[]" class="dropify" data-max-file-size="1M"/>
                                    </div>
                                </div>
                            @endfor

                        </div>

                        <div class="col-xs-12">
                           <div class="col-xs-2">
                               <div class="form-group">
                                   <label for="usernames">الملفات  </label>
                                   <input  type="file" required data-parsley-required-message="ﻭاﺣﺪﺓ ﻋﻠﻰ اﻷﻗﻞ  " name="files[]"  class="dropify" data-max-file-size="10M"/>

                               </div>
                        </div>

                            @for ($i = 0; $i < 1; $i++)
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label for="usernames">   الملفات  </label>
                                        <input type="file" name="files[]" class="dropify" data-max-file-size="10M"/>
                                    </div>
                                </div>
                            @endfor

                        </div>

                    </div>

                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    <script src="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>


    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {




            $('.js-example-basic-multiple').select2();

        });

        $('input[name=isOffer]').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.priceOffer').show();
            } else {
                $('.priceOffer').hide();
            }
        });

        $('input[name=dealOfToday]').on('click', function (e) {

            if ($(this).is(':checked')) {
                $('.dealField').show();
            } else {
                $('.dealField').hide();
            }
        });

        $( '.styled' ).one( "click", function() {
            var shortCutFunction = 'success';
            var msg = 'ﺳﻮﻑ ﻳﺘﻢ ﻣﻌﺎﻣﻠﺔ اﻟﻌﺮﺽ ﻋﻠﻲ اﻧﻪ ﻣﻦ ﺿﻤﻦ اﻟﻌﺮﻭﺽ اﻟﻴﻮﻣﻴﺔ ';
            var title = 'ﻧﺠﺎﺡ';
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;
        });




        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid()){
                $('.loading').show();

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                         if (data.status == false){

                             var shortCutFunction = 'error';
                             var msg = data.message;
                             var title = 'فشل';
                             toastr.options = {
                                 positionClass: 'toast-top-left',
                                 onclick: null
                             };
                             var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                             $toastlast = $toast;

                         }else {

                             var shortCutFunction = 'success';
                             var msg = data.message;
                             var title = 'ﻧﺠﺎﺡ';
                             toastr.options = {
                                 positionClass: 'toast-top-left',
                                 onclick: null
                             };
                             var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                             $toastlast = $toast;
                             setTimeout(function () {
                                 window.location.href = data.url;
                             }, 2000);

                        }


                    },
                    error: function (data) {
                        $('.loading').hide();

                    }
                });
            }else {
                $('.loading').hide();
            }
        });

    </script>
@endsection

