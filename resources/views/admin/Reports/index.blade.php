@extends('admin.layouts.master')
@section('title', 'إدارة الإشتراكات ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">


            </div>
            <h4 class="page-title"> قائمة     {{$pageName}} </h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box rotateOutUpRight ">

                <table id="datatable-responsive" class="table  table-striped">
                    <thead>
                    <tr>
                        <th>اسم التاجر  </th>
                        <th>رقم التاجر  </th>
                        <th>إيميل التاجر  </th>
                        <th>مجمل طلبات التاجر  </th>
                        <th>      تفاصيل    </th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($reports as $report)

                        <tr>

                            <td> {{$report->name}}  </td>
                            <td> {{$report->phone}}  </td>
                            <td> {{$report->email}}  </td>
                            <td> {{$report->orders_dealer_count}}  </td>
                            <td>
                                <a href="{{ route('reports-orders-show', $report->id) }}"
                                    data-toggle="tooltip" data-placement="top"
                                    data-original-title="@lang('institutioncp.show_details')"
                                    class="btn btn-icon btn-xs waves-effect  btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>


                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('#datatable-responsive').DataTable( {
                "order": [[ 2, "desc" ]]
            } );

        });
    </script>


@endsection

