@extends('admin.layouts.master')
@section('title', 'إدارة التقارير ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">


            </div>
            <h4 class="page-title"> قائمة تقارير   {{$pageName}} </h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box rotateOutUpRight ">


                <table id="datatable-fixed-header" class="table  table-striped">
                    <thead>
                    <tr>
                        <th>رقم الطلب</th>
                        <th>اسم المستخدم  </th>
                        <th>حالة الطلب    </th>
                        <th>مجمل سعر الطلب    </th>
                        <th>تاريخ   الطلب </th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($reports as $report)

                        <tr>
                            <td> {{$report->id}}  </td>
                            <td>  {{  optional( $report->user)->name}} </td>
                            <td>  {{  $report->status_translation}} </td>
                            <td>  {{  $report->total_price}} </td>
                            <td>  {{ $report->created_at ? $report->created_at->format('Y-m-d') :" لا يوجد تاريخ  " }} </td>

                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>


    </script>


@endsection

